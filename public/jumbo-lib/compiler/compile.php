<?php
// Načtení scriptu z loaderu
try {
	ob_flush();
	require_once __DIR__ . "/../loader.php";
	$script = ob_get_contents();
	ob_clean();
} catch (Exception $e) {
	die("Chyba při čtení dat ze souboru loader.php<br>\n");
}

// Minimalizace scriptu
try {
	require_once __DIR__ . "/minifier.php";
	$min = JShrink\Minifier::minify($script);
	$script = $min;
	unset($min);
} catch (Exception $e) {
	echo "Chyba při minimalizaci scriptu.<br>\n";
}

// Uložení scriptu
try {
	$f = fopen("jumbo.js", "w");
	fwrite($f, $script);
	fclose($f);

	echo "\"Zkompilovaný\" script byl vytvořen a uložen do souboru jumbo.js";
} catch (Exception $e) {
	die("Error: Chyba při vytváření souboru se \"zkompilovaným\" scriptem");
}