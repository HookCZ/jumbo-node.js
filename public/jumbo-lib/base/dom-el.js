/***************************************************
 * Jumbo - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: dom-el.js                       ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Namespace pro metody vytvářející html elementy
 * @namespace
 * @requires jumbo.ui
 */
jumbo.ui.el = {};

/**
 * Vytvoření DIV elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.div = function(a, cont) {
	return jumbo.ui.create("div", a, cont);
};

/**
 * Vytvoření SPAN elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.span = function(a, cont) {
	return jumbo.ui.create("span", a, cont);
};

/**
 * Vytvoření A elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.a = function(a, cont) {
	return jumbo.ui.create("a", a, cont);
};

/**
 * Vytvoření H1 elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.h1 = function(a, cont) {
	return jumbo.ui.create("h1", a, cont);
};

/**
 * Vytvoření H2 elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.h2 = function(a, cont) {
	return jumbo.ui.create("h2", a, cont);
};

/**
 * Vytvoření H3 elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.h3 = function(a, cont) {
	return jumbo.ui.create("h3", a, cont);
};

/**
 * Vytvoření P elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.p = function(a, cont) {
	return jumbo.ui.create("p", a, cont);
};

/**
 * Vytvoření STRONG elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.strong = function(a, cont) {
	return jumbo.ui.create("strong", a, cont);
};

/**
 * Vytvoření EM elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.em = function(a, cont) {
	return jumbo.ui.create("em", a, cont);
};

/**
 * Vytvoření HEADER elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.header = function(a, cont) {
	return jumbo.ui.create("header", a, cont);
};

/**
 * Vytvoření HGROUP elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.hgroup = function(a, cont) {
	return jumbo.ui.create("hgroup", a, cont);
};

/**
 * Vytvoření MAIN elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.main = function(a, cont) {
	return jumbo.ui.create("main", a, cont);
};

/**
 * Vytvoření NAV elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.nav = function(a, cont) {
	return jumbo.ui.create("nav", a, cont);
};

/**
 * Vytvoření FOOTER elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.footer = function(a, cont) {
	return jumbo.ui.create("footer", a, cont);
};

/**
 * Vytvoření SECTION elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.section = function(a, cont) {
	return jumbo.ui.create("section", a, cont);
};

/**
 * Vytvoření ARTICLE elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.article = function(a, cont) {
	return jumbo.ui.create("article", a, cont);
};

/**
 * Vytvoření IMG elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.el.img = function(a, cont) {
	return jumbo.ui.create("img", a, cont);
};