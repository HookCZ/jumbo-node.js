/***************************************************
 * Jumbo - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: teckova-notace.js               ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

if (jumbo.prototypeEdit) {
	/**
	 * Převede string na celé číslo
	 * @returns {Number}
	 */
	String.prototype.toInt = function() {
		return parseInt(this);
	};

	/**
	 * Převede string na desetinné číslo
	 * @returns {Number}
	 */
	String.prototype.toFloat = function() {
		return parseFloat(this);
	};

	/**
	 * Přídá event na HTML element
	 * @param {String} type
	 * @param {Function} fn
	 * @returns {Element | null}
	 */
	Element.prototype.addEvent = function(type, fn) {
		return jumbo.addEvent(this, type, fn);
	};

	Element.prototype.removeEvent = function(type, fn) {
		return jumbo.removeEvent(this, type, fn);
	};
}