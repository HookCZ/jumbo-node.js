/***************************************************
 * Jumbo - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: test.js                         ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Seznam testů, který se následně vyhodnotí
 * @type {{spravne: number, spatne: number, list: *[]}}
 */
jumbo.tests = {
	/**
	 * @private
	 */
	spravne: 0,

	/**
	 * @private
	 */
	spatne: 0,

	/**
	 * @private
	 */
	list: [
		{ nazev: null, testy: [] }
	],

	/**
	 * @private
	 */
	nepojmenovano: "Unnamed test",

	/**
	 * Přidá chybu do seznamu
	 * @param id
	 * @param text
	 * @private
	 */
	addFail: function(id, text) {
		this.spatne++;
		jumbo.tests.list[id].testy.push({stav: false, text: text || jumbo.nepojmenovano});
	},

	/**
	 * Přidá úspěch do seznamu
	 * @param id
	 * @param text
	 * @private
	 */
	addSuccess: function(id, text) {
		this.spravne++;
		jumbo.tests.list[id].testy.push({stav: true, text: text || jumbo.nepojmenovano});
	},

	/**
	 * Slouží k zobrazení výsledků
	 */
	print: function() {
		console.log("Unit testy: " + this.spravne + " testů bylo úspěšných a " + this.spatne + " testů bylo neúspěšných.");

//		console.log(this.list);

		for (var i = 1; i < this.list.length; i++) {
			console.log("Test: " + this.list[i].nazev);

			for (var j = 0; j < this.list[i].testy.length; j++) {
				console.log("\t" + (this.list[i].testy[j].stav ? "++" : "xx") + " " + this.list[i].testy[j].text);
			}
		}
	}
};

/**
 * Statická třída pro testování
 * @param {String} nazevTestu Název testu
 * @param {Function} f funkce s testy
 * @
 */
jumbo.test = function(nazevTestu, f) {
	var selfObj = {
		testID: jumbo.tests.list.length
	};

	jumbo.tests.list[selfObj.testID] = {
		nazev: nazevTestu,
		testy: []
	};

	/**
	 * Testuje hodnotu zda je true
	 * @param v Testovaná hodnota
	 * @param [text] Název nebo popisek konkrétního testu
	 * @returns {boolean}
	 */
	selfObj.assertTrue = function (v, text) {
		if (v === true) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text);
		return false;
	};

	/**
	 * Testuje hodnotu zda je false
	 * @param v Testovaná hodnota
	 * @param [text] Název nebo popisek konkrétního testu
	 * @returns {boolean}
	 */
	selfObj.assertFalse = function (v, text) {
		if (v === false) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text);
		return false;
	};

	/**
	 * Testuje zda jsou hodnoty shodné
	 * @param v První výraz
	 * @param h Druhý výraz
	 * @param [text] Název nebo popisek konkrétního testu
	 * @returns {boolean}
	 */
	selfObj.assertEquals = function (v, h, text) {
		if (jumbo.isEquals(v, h)) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text + "(Očekáváno: ,," + h + "\" obdrženo: ,," + v + "\")");
		return false;
	};

	/**
	 * Testuje zda je hodnota null
	 * @param v Testovaná hodnota
	 * @param text
	 * @returns {boolean}
	 */
	selfObj.assertNull = function (v, text) {
		if (typeof v !== "undefined" && v === null) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text);
		return false;
	};

	/**
	 * Testuje zda hodnota není null
	 * @param v Testovaná hodnota
	 * @param text
	 * @returns {boolean}
	 */
	selfObj.assertNotNull = function (v, text) {
		if (typeof v !== "undefined" && v !== null) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text);
		return false;
	};

	/**
	 * Zapíše vlastní chybu
	 * @param text
	 */
	selfObj.fail = function(text) {
		jumbo.tests.addFail(selfObj.testID, text);
	};

	/**
	 * Zapíše vlastní úspěch
	 * @param text
	 */
	selfObj.success = function(text) {
		jumbo.tests.addSuccess(selfObj.testID, text);
	};

	f.call(selfObj);
};

module.exports = jumbo;