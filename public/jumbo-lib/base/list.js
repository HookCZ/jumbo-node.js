/***************************************************
 * Jumbo - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: list.js                         ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Třída pro vytvoření listu s libovolným identifikátorem
 * @class
 * @type {*}
 */
jumbo.List = _class({
	/**
	 * @constructor
	 */
	constructor: function() {
//		this.list = [];
		this.table = {};
		this.length = 0;
	},

	/**
	 * Vytváří stringové hashe pro klíče
	 * @param obj
	 * @returns {string}
	 * @private
	 */
	getHash: function(obj) {
		var hash = obj.toString();

		if (hash.length > 20) {
			hash = hash.substr(0, 20);
		}

		return hash;
	},

	/**
	 * Vrátí index hledané položky
	 * @param id
	 * @param hash
	 * @private
	 */
	getIdOf: function(id, hash) {
		if (typeof this.table[hash] == "undefined") {
			return null;
		}

		for (var i = 0; i < this.table[hash].length; i++) {
			if (this.table[hash][i].index == id) {
				return i;
			}
		}

		return null;
	},

	/**
	 * Vložení položky do listu. Není možné vložit duplicitní identifikátor
	 * @param {Object} id Identifikátor položky
	 * @param {Object} value Ukládaná hodnota
	 * @return {Boolean}
	 */
	add: function(id, value) {
		var hash = this.getHash(id);

		if (this.getIdOf(id, hash) == null) {
			if (typeof this.table[hash] == "undefined") {
				this.table[hash] = [];
			}

			this.table[hash].push({
				index:	id,
				val:	value
			});

			this.length++;

			return true;
		} else {
			return false;
		}
	},

	/**
	 * Odstraní danou položku ze seznamu
	 * @param {Object} id Identifikátor položky
	 * @returns {null | Object}
	 */
	remove: function(id) {
		var hash = this.getHash(id);

		if (typeof this.table[hash] != "undefined") {
			var i = this.getIdOf(id, hash);

			if (i != null) {
				this.length--;
				return this.table[hash].splice(i, 1);
			}
		}

		return null;
	},

	/**
	 * Vybrání dané položky
	 * @param {Object} id Identifikátor položky
	 * @returns {null | Object}
	 */
	get: function(id) {
		var hash = this.getHash(id);

		if (typeof this.table[hash] != "undefined") {
			var i = this.getIdOf(id, hash);

			if (i != null) {
				return this.table[hash][i].val;
			}
		}

		return null;
	},

	/**
	 * Změna hodnoty u dané položky
	 * @param {Object} id Identifikátor položky
	 * @param {Object} value Hodnota
	 * @returns {boolean}
	 */
	set: function(id, value) {
		var hash = this.getHash(id);

		if (typeof this.table[hash] != "undefined") {
			var i = this.getIdOf(id, hash);

			if (i != null) {
				this.table[hash][i].val = value;
				return true;
			}
		}

		return false;
	}
});