/***************************************************
 * Jumbo - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: dom.js                          ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Namespace pro metody upravující UI -> DOM, okna, efekty, aj.
 * @namespace
 */
jumbo.ui = {};


/**
 * Ověřuje zda daný objekt je html element
 * @param {Object} el Object k ověření
 * @returns {Boolean}
 */
jumbo.ui.isNode = function(el) {
	if (typeof el != "object") {
		return false;
	}

	if (window.Node) {
		return el instanceof Node;
	} else {
		return typeof el.innerHTML == "string";
	}
};

/**
 * Vytvoření html elementu
 * @param {String} tag
 * @param {Object | String} params
 * @param {Object | String} [cont] Obsah elementu
 * @returns {HTMLElement}
 */
jumbo.ui.create = function(tag, params, cont) {
	var el = null;

	if (typeof tag == "string") {
		if (tag.substr(0, 1) != "<") {
			//var el;
			if (tag == "svg") {
				if (document.createElementNS) {
					el = document.createElementNS("http://www.w3.org/2000/svg", "svg");
				} else {
					el = document.createElement("svg");
				}
				el.setAttribute("version", "1.2");
			} else {
				el = document.createElement(tag);
			}

			var tmp;

			if (typeof params != "object") {
				params = jumbo.ui.getAttributesAsObject(params);
			} else if (params == null) {
				params = {};
			}

			if (typeof params == "object") {

				if (typeof params.content == "undefined") {
					params.content = cont || "";
				}

				for (var a in params) {
					if (params.hasOwnProperty(a)) {
						if (a == "content") {
							if (typeof params[a] == "object" && !!params[a].tagName) {
								el.appendChild(params[a]);
							} else if (Object.prototype.toString.call(params[a]) == "[object Array]") {
								for (var e = 0; e < params[a].length; e++) {
									if (typeof params[a][e] == "object" && !!params[a][e].tagName) {
										el.appendChild(params[a][e]);
									} else {
										tmp = document.createElement(tag);
										tmp.innerHTML = params[a][e];

										var l = tmp.childNodes.length;

										for (var ch = 0; ch < l; ch++) {
											el.appendChild(tmp.childNodes[0]);
										}

//										delete tmp;
									}
								}
							} else {
								el.innerHTML = params[a];
							}
						} else {
							el.setAttribute(a, params[a]);
						}
					}
				}
			}
		} else {
			el = document.createElement("div");
			el.innerHTML = tag;
//			el = el.firstChild;
			el = el.childNodes;
		}
	}

	return el;
};

/**
 * Převod textové reprezentace atributů na objektovou
 * @param {String} attributes
 */
jumbo.ui.getAttributesAsObject = function(attributes) {
	if (typeof attributes != "string") {
		if (jumbo.consoleLogging === true) {
			console.log("Z takového vstupu není možné sestavit argumenty.");
		}
		return {};
	}

	var obj = {};
	var attrs, i;

	if (jumbo.attrsTextRepresentation == "CSV") {
		var key, value;
		attrs = attributes.split(",");

		for (i = 0; i < attrs.length; i++) {
			key = attrs[i].split("=", 1)[0];
			value = attrs[i].substr(key.length + 1);

			if (key.match(/^[a-zA-Z-]+$/)) {
				obj[key] = value;
			} else {
				if (jumbo.consoleLogging === true) {
					console.log("Název atributu \"" + key + "\" je chybný");
				}
			}
		}
	} else if (jumbo.attrsTextRepresentation == "HTML") {
		attributes.replace('"', "'");
		attrs = attributes.split("' ");

		// Z posledního atributu odstraníme apostrof na konci
		var lastIndex = attrs.length - 1;
		var last = attrs[lastIndex];
		attrs[lastIndex] = last.substr(0, last.length - 1);

		var keyValue;

		for (i = 0; i < attrs.length; i++) {
			keyValue = attrs[i].split("='");

			if (keyValue[0].match(/^[a-zA-Z-]+$/)) {
				obj[keyValue[0]] = keyValue[1];
			} else {
				if (jumbo.consoleLogging === true) {
					console.log("Název atributu \"" + keyValue[0] + "\" je chybný");
				}
			}
		}
	}

	return obj;
};
