/***************************************************
 * Jumbo - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: loader.php                      ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/
/**
 * Statická třída pro jumbo knihovnu, která zapouzdřuje všechny ostatní prvky
 */
var jumbo = {
	prototypeEdit:				true,
	consoleLogging:				true,
	performanceOptimalization:	false,
	attrsTextRepresentation:	"CSV" // HTML | CSV
};

var $j = jumbo;

/**
 * Uložiště privátních dat
 * @returns {Object}
 * @private
 */
jumbo.privateData = function() {
	return {
		version:	"0.1.0.0a",
		author:		"Roman Jámbor"
	};
};

/**
 * Vrací verzi knihovny
 * @returns string
 */
jumbo.getVersion = function() {
	return jumbo.privateData().version;
};

/**
 * Vrátí autora knihovny
 * @returns string
 */
jumbo.getAuthor = function() {
	return jumbo.privateData().author;
};

/**
 * CSS-like selector
 * @param {String} query Výběrový řetězec
 * @param [from]
 * @returns {*}
 */
jumbo.get = function(query, from) {
	from = from || document;
	var w = query.substr(0, 1);

	if (w == "#") {
		return from.querySelector(query);
	} else {
		return from.querySelectorAll(query);
	}
};

///**
// * Privátní uložiště pro data
// * @private
// * @type {Array}
// */
//jumbo.dataStorage = {};
//
///**
// * Metoda pro ukládání a výběr dat, které nešpiní globální scope
// * @param {String | Object} key Klíč pod kterým budou data uložena
// * @param {Object | String} [data] Data, která mají být uložena
// * @returns {*}
// * @example
// * // Uložení a vybrání hodnoty
// * jumbo.data("mujKlic", [1, 2, 3]);
// * console.log(jumbo.data("mujKlic")[1]); // Vypíše 2
// *
// * // Ukládají se zde i systémová data, např. informace o nastavených eventech
// * // Zjištění přítomnosti eventu na elementu
// * // Neplatí jumbo.data(document.body, "events"); // Vrátí např. {click: [Function, Function], load:[Function]}
// */
//jumbo.data = function(key, data) {
//	var isArray = false;
//
//	if (typeof key == "string" && typeof data != "undefined") { // Vkládáme
//		jumbo.dataStorage[key] = data;
//	} else if (typeof key == "object" && data == "events") { // Chceme eventy
//
//	} else if (typeof key == "string" && !data) { // Vybíráme
//		return jumbo.dataStorage[key];
//	}
//
////	if (typeof key == "string") {
////		if (key.length > 2) {
////			if (key.substr(-2) == "[]") {
////				isArray = true;
////				key = key.substr(0, key.length - 2);
////			}
////		}
////	}
////
////	if (typeof data == "undefined") { // Výběr dat
//////		if (isArray) {
////		if (key == "events") {
////
////		} else {
////			return jumbo.dataStorage[key];
////		}
////	} else { // Uložení dat
////		if (isArray) {
////			if (!!jumbo.dataStorage[key]) {
////				jumbo.dataStorage[key].push(data);
////			} else {
////				jumbo.dataStorage[key] = [data];
////			}
////		} else {
////			jumbo.dataStorage[key] = data;
////		}
////	}
//};

/**
 * Metoda pro porovnání rovnosti dvou prvků
 * @param a
 * @param b
 * @returns {boolean}
 */
jumbo.isEquals = function(a, b) {
	if (typeof a === "undefined" || typeof b === "undefined" || a === null || b === null) {
		return false;
	}

	if (typeof a !== typeof b) {
		return false;
	} else if (typeof a !== "object") {
		return a === b;
	}

	// Dále pokračujeme pouze pokud se jedná o objekty

	// Konstruktory se musejí shodovat
	if (a.constructor !== b.constructor) {
		return false;
	}

	// Všechny prvky z a musejí být v b a zároveň se musejí jejich hodnoty rovnat
	for (var i in a) {
		if (a.hasOwnProperty(i)) {
//			console.log(typeof i);
			if (!b.hasOwnProperty(i)) return false;
			if (!jumbo.isEquals(a[i], b[i])/*a[i] != b[i]*/) return false;
		}
	}

	// Všechny prvky z b musejí být v a a jejich hodnoty se musejí rovnat
	for (i in b) {
		if (b.hasOwnProperty(i)) {
			if (!a.hasOwnProperty(i)) return false;
			if (!jumbo.isEquals(a[i], b[i])/*a[i] != b[i]*/) return false;
		}
	}

	return true;
};

/**
 * Přídá event na HTML element
 * @param {HTMLElement} el HTML element na který chceme event přidat
 * @param {String} type Event, který chceme přidat [click, mousemove, load, ...]
 * @param {Function} fn Funkce, která bude zavolána při spuštění události
 * @returns {HTMLElement | null}
 */
jumbo.addEvent = function(el, type, fn) {
	if (typeof el != "object") {
		if (jumbo.consoleLogging) console.log("Dosazený prvek není objekt.");
		return null;
	}

	if (el.addEventListener) {
		el.addEventListener(type, fn);
	} else if (el.attachEvent) {
		el.attachEvent("on" + type, fn);
	} else {
		if (jumbo.consoleLogging) console.log("Event '" + type + "' není možné na daný prvek nastavit.");
		return null;
	}

	return el;
};

/**
 *
 * @param {HTMLElement} el HTML element na který chceme event přidat
 * @param {String} type Event, který chceme přidat [click, mousemove, load, ...]
 * @param {Function} fn Funkce, která bude zavolána při spuštění události
 * @returns {HTMLElement | null}
 */
jumbo.removeEvent = function(el, type, fn) {
	if (typeof el != "object") {
		if (jumbo.consoleLogging) console.log("Dosazený prvek není objekt.");
		return null;
	}

	if (el.removeEventListener) {
		el.removeEventListener(type, fn);
	} else if (el.detachEvent) {
		el.detachEvent("on" + type, fn);
	} else {
		if (jumbo.consoleLogging) console.log("Event '" + type + "' není možné na daný prvek nastavit.");
		return null;
	}

	return el;
};

/**
 * Udává stav dokumentu - načtený/nenačtený
 * @type {boolean}
 */
jumbo.applicationReady = false;

/**
 * Zavolá funkci po načtení stránky
 * @param {Function} fn
 * @param {Number} [delay] Možné akci zpozdit ještě po načtení
 * @returns {boolean}
 */
jumbo.ready = function(fn, delay) {
	if (typeof fn != "function") {
		if (jumbo.consoleLogging) console.log("Dosazený parametr není funkce.");
		return false;
	}

	var f;

	if (!!delay) {
		f = function(e) {
			e = e || window.event;
			setTimeout(function(e) {
				e = e || window.event;
				fn(e);
			}, delay);
		};
	} else {
		f = fn;
	}

	if (jumbo.applicationReady === true) {
		f();
		return true;
	}

	// Pokud ještě nebylo vytvořeno pole pro naše volání, tak jej vytvoříme
	if (!jumbo.memForOnReadyCalls) {
		jumbo.memForOnReadyCalls = [function() {
			jumbo.applicationReady = true;
		}];

		jumbo.addEvent(window, "load", function(e) {
			e = e || window.event;
			jumbo.callOnReadyFunctions(e);
		});
	}

	this.memForOnReadyCalls.push(f);

	return true;
};

/**
 * Volá všechny uložené funkce
 * @param e
 * @private
 */
jumbo.callOnReadyFunctions = function(e) {
	for (var i = 0; i < jumbo.memForOnReadyCalls.length; i++) {
		jumbo.memForOnReadyCalls[i](e);
	}
};
