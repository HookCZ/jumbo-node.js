/***************************************************
 * Jumbo - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: oop.js                          ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Slouží pro pohodlnější vytváření tříd
 * @param {Object} defs
 * @return {Function}
 * @example
	var Serializable = {
		serialize:	function() {
			return "serialized data...";
		}
	};

	// Parrent class
	var MyClassParent = _class({
	constructor:	function(name) {
		this.name = name;
	},
	myMethod:		function() {
		return "Parent's name: " + this.name;
	},
	setName:		function(name) {
		this.name = name;
	}
	});

	// Child class
	var ParentsChild = _class({
		constructor:	function(name, age) {
			this._super(name); // Call parent's constructor
			this.age = age;
		},
		myMethod:		function() { // Overwrite
			return "Child's name: " + this.name;
			// MyClassParent._super.myMethod.call(this); can be called to run Parent's overwritten method.
		},
		setAge:			function(age) {
			this.age = age;
		}
	}).Extends(MyClassParent).Implements(Serializable);

	var child = new ParentsChild("Foo", 20);
	console.log(child instanceof ParentsChild, child instanceof MyClassParent, child.implementationOf(Serializable), child.myMethod());
 */
var _class = function(defs) {
	// Pokud definice obsahují konstruktor, vytvoříme jej, jinak použijeme prázdnou fci
	var c = defs.hasOwnProperty("constructor") ? defs.constructor : function() { };

	/**
	 * Pole zaznamenávající které objekty třída implementuje
	 * @type {Array}
	 * @private
	 */
	c.prototype.implementations = [];

//	var implementations = [];

	/**
	 * Ověřuje, zda třída používá dané rozhraní
	 * @param o
	 * @return {boolean}
	 */
	c.prototype.implementationOf = function(o) {
		if (o !== defs) {
			if (this.implementations.indexOf(o) != -1) {
				return true;
			}
		}

		return false;
	};

	/**
	 * Slouží k podědění třídy
	 * @param parent {Function} Třída
	 * @return {constructor}
	 */
	c.Extends = function(parent) {
		if (typeof parent === "function") {
			var Tmp = function() { };
			Tmp.prototype = parent.prototype;
			c._super = Tmp.prototype;
			c.prototype = new Tmp();
			c.prototype._super = function() {
				parent.apply(this, arguments);
			};

			// Musíme znovu nabalit vlastní metody, protože podědění nám smaže existující prototype - bohužel je to nevýhoda pozdního volání Extends
			c.Implements(defs);

			// Aktualizujeme
			c.prototype.constructor = c;
		}

		return c;
	};

	/**
	 * Slouží k převzení prvků z objektu/interface. Jedná se o mixing což se dá nazvat jako implementace rozhraní.
	 * @param obj {Object} Objekt/interface
	 * @return {constructor}
	 */
	c.Implements = function(obj) {
		if (obj !== defs) {
			if (c.prototype.implementations.indexOf(obj) != -1) {
				return c;
			}

			c.prototype.implementations.push(obj);
		}

		// Přidáme vlastnosti na náš prototype
		for (var k in obj) {
			if (obj.hasOwnProperty(k) && k != "constructor") {
				if (typeof c.prototype[k] != "undefined" && obj !== defs) {
					continue;
				}
				c.prototype[k] = obj[k];
			}
		}

		return c;
	};

	// Nabalíme požadované metody na konstruktor, využijeme načí metody Implements
	c.Implements(defs);

	// Nastavíme constructor na konstruktor
	c.prototype.constructor = c;

	return c;
};