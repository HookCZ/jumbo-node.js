/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: effects.js                      ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/


/**
 * Skryje Node
 * @param {Node} el
 * @returns {Node}
 */
jumbo.hide = function(el) {
	if (!jumbo.isNode(el)) {
		if (jumbo.consoleLogging === true) console.log("Dosazený object není Node");
		return el;
	}

	el.style.display = "none";
	el.style.visibility = "hidden";

	return el;
};


/**
 * Zobrazí Node
 * @param {Node} el
 * @returns {Node}
 */
jumbo.show = function(el) {
	if (!jumbo.isNode(el)) {
		if (jumbo.consoleLogging === true) console.log("Dosazený object není Node");
		return el;
	}

	el.style.opacity = "1";
	el.style.display = "block";
	el.style.visibility = "visible";

	if (window.attachEvent) { // if IE
		el.style.filter = "alpha(opacity=100)";
	}

	return el;
};


/**
 * Zjistí, jestli je element viditelný nebo ne
 * @param {Node} el
 * @returns {boolean}
 */
jumbo.isVisible = function(el) {
	if (!jumbo.isNode(el)) {
		if (jumbo.consoleLogging === true) console.log("Dosazený object není Node");
		return false;
	}

	if (el.style.display == "" && el.style.visibility == "") {
		return (window.getComputedStyle(el).display === 'block');
	}

	return el.style.display != "none" && el.style.visibility != "hidden" && el.style.opacity > 0;
};


/**
 * Zjistí, jestli je element skryt nebo ne
 * @param {Node} el
 * @returns {boolean}
 */
jumbo.isHidden = function(el) {
	return !jumbo.isVisible(el);
};


/**
 * Paměť pro elementy, se kterýma právě něco děláme, abychom nemohli provádět 2 akce současně
 * @private
 * @type {Array}
 */
jumbo.fadingElementsMem = [];


/**
 * Prolínáním zobrazí nebo skryje element
 * @param el
 * @param time
 * @param [func]
 * @param fadeInTrue
 * @returns {Node}
 * @private
 */
jumbo.fade = function(el, time, func, fadeInTrue) {
	if (!jumbo.isNode(el)) {
		if (jumbo.consoleLogging === true) console.log("Dosazený object není Node");
		return el;
	}

	if (jumbo.fadingElementsMem.indexOf(el) != -1) {
		if (jumbo.consoleLogging === true) console.log("Na tomto elementu již probíhá fade efekt.");
		return el;
	}

	jumbo.fadingElementsMem.push(el);

	time = time || 300;

	var step = 0.1;
	var tick = Math.round(time / 10); // 10 - počet přechodů

	if (jumbo.performanceOptimalization === false) {
		step = 1 / (time / 40);
		tick = 40;

		if (step < 0.01) {
			step = 0.01;
			tick = time / 100;
		}
	}

	var opacity;
	var interval;
	var isIE = !!window.attachEvent;
	var i = fadeInTrue ? 1 : 0;
	var val = [
		{start: 1, stop: 0}, // fadeOut
		{start: 0, stop: 1} // fadeIn
	];
	var decInc;
	var limitReached;

	if (fadeInTrue) {
		decInc = function(op) {
			return op + step;
		};
		limitReached = function(op) {
			return op > 1 - step;
		};
	} else {
		decInc = function(op) {
			return op - step;
		};
		limitReached = function(op) {
			return op < step;
		};
	}

	jumbo.show(el);

	el.style.opacity = val[i].start;
	if (isIE) el.style.filter = "alpha(opacity=" + val[i].start * 10 + ")";

	interval = setInterval(function() {
		opacity = /*Math.round((*/parseFloat(el.style.opacity)/*) * 100) / 100*/;

		if (limitReached(opacity)) {
			clearInterval(interval);

			el.style.opacity = val[i].stop;
			if (isIE) el.style.filter = "alpha(opacity=" + val[i].stop * 10 + ")";

			jumbo.fadingElementsMem.splice(jumbo.fadingElementsMem.indexOf(el), 1);

			if (typeof func === "function") {
				func();
			}

			if (!fadeInTrue) {
				jumbo.hide(el);
			}
		} else {
			opacity = decInc(opacity);
			el.style.opacity = opacity;
			if (isIE) el.style.filter = "alpha(opacity=" + (opacity * 10) + ")";
		}
	}, tick);

	return el;
};


/**
 * Prolínáním zobrazí element
 * @param {Node} el
 * @param {Number} [time]
 * @param {Function} [func]
 * @returns {Node}
 */
jumbo.fadeIn = function(el, time, func) {
	return jumbo.fade(el, time, func, true);
};


/**
 * Prolínáním skryje element
 * @param {Node} el
 * @param {Number} [time]
 * @param {Function} [func]
 * @returns {Node}
 */
jumbo.fadeOut = function(el, time, func) {
	return jumbo.fade(el, time, func, false);
};


jumbo.rollingElementsMem = [];


/**
 * Sroluje element - postupně zmenšuje height
 * @param el
 * @param [time]
 * @returns {*}
 */
jumbo.roll = function(el, time) {
	if (!el || jumbo.rollingElementsMem.indexOf(el) != -1) {
		return;
	}

	var height = el.offsetHeight;
	el.overflow = "hidden";

	if (height > 0) {
		jumbo.rollingElementsMem.push(el);

		time = time || 300;

		var tick;
		var step;

		if (jumbo.performanceOptimalization === false) {
			tick = 10;
			step = height / (time / tick);
		} else {
			tick = 40;
			step = height / (time / tick);
		}

		if (step < 1) {
			step = 1;
			tick = time / height;
		}

		var interval = setInterval(function() {
			height -= step;

			if (height <= 0) {
				clearInterval(interval);
				delete jumbo.rollingElementsMem[jumbo.rollingElementsMem.indexOf(el)];
				el.style.height = "0px";
				return;
			}

			el.style.height = height + "px";
		}, tick)
	}

	return el;
};


/**
 * Rozbalí element - postupně zvětší height
 * @param el
 * @param [time]
 * @returns {*}
 */
jumbo.unroll = function(el, time) {
	if (!el || jumbo.rollingElementsMem.indexOf(el) != -1) {
		return;
	}

	var height = el.offsetHeight;
	var unrollTo = el.scrollHeight;
	var restHeight = unrollTo - height;

	if (height <= 0) {
		jumbo.rollingElementsMem.push(el);
		time = time || 300;

		var tick;
		var step;

		if (jumbo.performanceOptimalization === false) {
			tick = 10;
			step = restHeight / (time / tick);
		} else {
			tick = 40;
			step = restHeight / (time / tick);
		}

		if (step < 1) {
			step = 1;
			tick = time / restHeight;
		}

		var interval = setInterval(function() {
			height += step;

			if (height >= unrollTo) {
				clearInterval(interval);
				el.style.height = unrollTo + "px";
				delete jumbo.rollingElementsMem[jumbo.rollingElementsMem.indexOf(el)];
				return;
			}

			el.style.height = height + "px";
		}, tick)
	}

	return el;
};

//
///**
// * Animovaný posun dolů
// * @param el
// * @param dist
// * @param speed number - 0.1 - x, optimálně 1
// * @param [func]
// */
//jumbo.slideDown = function(el, dist, speed, func) {
//	var pos = jumbo.setAbsPos(el);
//
//	if (func === undefined) {
//		func = function() {};
//	}
//
//	// Aktivujeme animaci
//	jumbo.doSlideMove(el, 1, pos.y + dist, speed, func);
//};
//
//// Pro objektové volání s tečkovou notací u objektu
//if (!Object.slideDown && jumbo.pridanaTeckovaNotace) {
//	/**
//	 * Animovaný posun dolů
//	 * @param d Vzdálenost
//	 * @param s Časová konstanta rychlosti - 0.1 - x, optimálně 1
//	 * @param [func]
//	 */
//	Object.prototype.slideDown = function(d, s, func) {
//		if (func === undefined) {
//			func = function() {};
//		}
//
//		jumbo.slideDown(this, d, s, func);
//	}
//}
//
///**
// * Animovaný posun nahoru
// * @param el
// * @param dist
// * @param speed number - 0.1 - x, optimálně 1
// * @param [func]
// */
//jumbo.slideUp = function(el, dist, speed, func) {
//	var pos = jumbo.setAbsPos(el);
//
//	if (func === undefined) {
//		func = function() {};
//	}
//
//	// Aktivujeme animaci
//	jumbo.doSlideMove(el, 2, pos.y - dist, speed, func);
//};
//
//// Pro objektové volání s tečkovou notací u objektu
//if (!Object.slideUp && jumbo.pridanaTeckovaNotace) {
//	/**
//	 * Animovaný posun nahoru
//	 * @param d
//	 * @param s
//	 * @param [func]
//	 */
//	Object.prototype.slideUp = function(d, s, func) {
//		if (func === undefined) {
//			func = function() {};
//		}
//
//		jumbo.slideUp(this, d, s, func);
//	}
//}
//
///**
// * Animovaný posun doleva
// * @param el
// * @param dist
// * @param speed number - 0.1 - x, optimálně 1
// * @param [func]
// */
//jumbo.slideLeft = function(el, dist, speed, func) {
//	var pos = jumbo.setAbsPos(el);
//
//	if (func === undefined) {
//		func = function() {};
//	}
//
//	// Aktivujeme animaci
//	jumbo.doSlideMove(el, 3, pos.x - dist, speed, func);
//};
//
//// Pro objektové volání s tečkovou notací u objektu
//if (!Object.slideLeft && jumbo.pridanaTeckovaNotace) {
//	/**
//	 * Animovaný posun doleva
//	 * @param d Vzdálenost
//	 * @param s Časová konstanta rychlosti - 0.1 - x, optimálně 1
//	 * @param [func]
//	 */
//	Object.prototype.slideLeft = function(d, s, func) {
//		if (func === undefined) {
//			func = function() {};
//		}
//
//		jumbo.slideLeft(this, d, s, func);
//	}
//}
//
///**
// * Animovaný posun doprava
// * @param el
// * @param dist
// * @param speed number - 0.1 - x, optimálně 1
// * @param [func]
// */
//jumbo.slideRight = function(el, dist, speed, func) {
//	var pos = jumbo.setAbsPos(el);
//
//	if (func === undefined) {
//		func = function() {};
//	}
//
//	// Aktivujeme animaci
//	jumbo.doSlideMove(el, 4, pos.x + dist, speed, func);
//};
//
//// Pro objektové volání s tečkovou notací u objektu
//if (!Object.slideRight && jumbo.pridanaTeckovaNotace) {
//	/**
//	 * Animovaný posun doprava
//	 * @param d Vzdálenost
//	 * @param s Časová konstanta rychlosti - 0.1 - x, optimálně 1
//	 * @param [func]
//	 */
//	Object.prototype.slideRight = function(d, s, func) {
//		if (func === undefined) {
//			func = function() {};
//		}
//
//		jumbo.slideRight(this, d, s, func);
//	}
//}
//
///**
// * Vykonání posunu
// * @param el
// * @param dir
// * @param fin
// * @param speed
// * @private
// * @param func
// */
//jumbo.doSlideMove = function(el, dir, fin, speed, func) {
//	var pos = {
//		x:	parseInt(el.style.left),
//		y:	parseInt(el.style.top)
//	};
//	//jumbo.absPos(el);
//
//	if (func === undefined) {
//		func = function() {};
//	}
//
//	// Spočítáme posunutí v závislosti na směru
//	var n;
//	if (dir == 1) {
//		n = pos.y + (10 * speed);
//	} else if (dir == 2) {
//		n = pos.y - (10 * speed);
//	} else if (dir == 3) {
//		n = pos.x - (10 * speed);
//	} else {
//		n = pos.x + (10 * speed);
//	}
//	if (((dir == 1 || dir == 4) && n > fin) || ((dir == 2 || dir == 3) && n < fin)) {
//		n = fin;
//	}
//
//	// Provedeme posun
//	if (dir == 1 || dir == 2) {
//		el.style.top = n + "px";
//	} else {
//		el.style.left = n + "px";
//	}
//
//	// Pokud je to potřeba, zopakujeme posun
//	if (((dir == 1 || dir == 4) && n < fin) || ((dir == 2 || dir == 3) && n > fin)) {
//		setTimeout(function () {
//			jumbo.doSlideMove(el, dir, fin, speed, func);
//		}, 40);
//	} else { // Pohyb dokončen
//		func();
//	}
//};
//



/**
* Vrátí pozici scrollbaru u dokumentu nebo u zadaného elementu
* @param {Node} [el]
* @returns {[number, number]}
*/
jumbo.getScroll = function(el) {
	el = el || document;

	if (!el && window.pageYOffset) {
		return [window.pageXOffset, window.pageYOffset];
	} else {
		return [el.scrollLeft || document.documentElement.scrollLeft || 0, el.scrollTop || document.documentElement.scrollTop || 0];
	}
};


//
///**
//* Odskroluje na určitou pozici na stránce
//* @param pos Počet pixelů od vrchu
//* @param [time] Doba trvání animace
//* @param [el] Element, ve kterém má ke scrollu dojít - default document.body
//*/
//jumbo.scrollTop = function(pos, time, el) {
//	if (el === undefined) {
//		el = document.body;
//	}
//
//	var curPos = jumbo.getScroll(el)[1];
//	var dist = pos - curPos;
//	var steps;
//
//	if (time !== undefined) {
//		steps = Math.round(time / 40);
//	} else {
//		steps = 12;
//	}
//
//	var step = Math.round(dist / steps);
//	var iter = 0;
//
//	var int = setInterval(function() {
//		if (iter == steps - 1/*curPos + step >= pos || last == curPos *//*curPos >= el.scrollHeight - el.clientHeight*/) {
//			curPos = pos;
//			//window.scrollTo(0, pos);
//			clearInterval(int);
//		} else {
//			curPos += step;
//		}
//
//
//		if (el == document.body) {
//			window.scrollTo(0, curPos);
//		} else {
//			el.scrollTop = curPos;
//		}
//
//		iter++;
//	}, 40);
//};