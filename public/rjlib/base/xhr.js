/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: xhr.js                          ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Odešle asynchronní XmlHttpRequest
 * @param {Object} data {
	 * 	url
	 * 	method
	 * 	data
	 * 	callback
	 * 	user
	 * 	password
	 * 	contentType
	 * 	responseType
 * 	}
 */
jumbo.ajax = function(data) {
	if (typeof data != "object" && !data.url && !data.method) {
		if (jumbo.consoleLogging) console.log("Bad input data for jumbo.ajax()");
		return;
	}

	var xhr = (XMLHttpRequest) ? (new XMLHttpRequest()) : ((new ActiveXObject("Microsoft.XMLHTTP")) || null);

	if (xhr == null) {
		if (jumbo.consoleLogging) console.log("Your browser doesn't support XHR!");
		return;
	}

	xhr.open(data.method, data.url, true, data['user'] || "", data['password'] || "");
	xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

	if (data['contentType']) {
		xhr.setRequestHeader("Content-Type", data['contentType']);
	}

	if (data['responseType']) {
		xhr.responseType = data['responseType'];
	}

	var callbackCalled = false;

	xhr.addEventListener("error", function() {
		callbackCalled = true;
		if (data['callback']) {
			data['callback']({
				code: xhr.status,
				message: "XHR error occurs"
			}, xhr.response);
		}
	});
	xhr.addEventListener("abort", function() {
		callbackCalled = true;
		if (data['callback']) {
			data['callback']({
				code: xhr.status,
				message: "Request was aborted"
			}, xhr.response);
		}
	});

	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			if (xhr.status >= 200 && xhr.status < 300 && data['callback']) {
				data['callback'](null, xhr.response);
			} else if (xhr.status >= 400 && xhr.status < 500 && data['callback'] && !callbackCalled) {
				data['callback']({
					code: xhr.status,
					message: "Response with status code " + xhr.status
				}, xhr.response);
			}
		}
	};

	xhr.send(data['data'] || null);
};


jumbo.xhr = {
	/**
	 * Vyšle POST požadavek
	 * @param url
	 * @param data
	 * @param callback
	 * @param [responseType]
	 */
	post: function(url, data, callback, responseType) {
		var setup = {
			url: url,
			method: "POST",
			responseType: responseType || null,
			callback: callback
		};

		if (data instanceof FormData) {
			setup.data = data;
		} else {
			try {
				setup.data = JSON.stringify(data);
				setup.contentType = "application/json";
			} catch (ex) { setup.data = data; }
		}

		jumbo.ajax(setup);
	},


	/**
	 * Vyšle GET požadavek
	 * @param url
	 * @param callback
	 * @param [responseType]
	 */
	get: function(url, callback, responseType) {
		jumbo.ajax({
			url: url,
			method: "GET",
			responseType: responseType || null,
			callback: callback
		});
	},


	/**
	 * Vyšle PUT požadavek
	 * @param url
	 * @param data
	 * @param callback
	 * @param [responseType]
	 */
	put: function(url, data, callback, responseType) {
		var setup = {
			url: url,
			method: "PUT",
			responseType: responseType || null,
			callback: callback
		};

		try {
			setup.data = JSON.stringify(data);
			setup.contentType = "application/json";
		} catch (ex) { setup.data = data; }

		jumbo.ajax(setup);
	},


	/**
	 * Vyšle DELETE požadavek
	 * @param url
	 * @param callback
	 * @param [responseType]
	 */
	delete: function(url, callback, responseType) {
		jumbo.ajax({
			url: url,
			method: "DELETE",
			responseType: responseType || null,
			callback: callback
		});
	}
};


jumbo.sendForm = function(form, callback) {
	jumbo.ajax({
		url: form.action,
		method: "POST",
		//contentType: "multipart/form-data",
		data: new FormData(form),
		callback: callback
	});
};