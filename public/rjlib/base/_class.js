/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: _class.js                       ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Slouží pro pohodlnější vytváření tříd
 * @param {Object} defs
 * @return {Function}
 */
var _class = function(defs) {
	// Pokud definice obsahují konstruktor, vytvoříme jej, jinak použijeme prázdnou fci
	var c = defs.hasOwnProperty("constructor") ? defs.constructor : defs.hasOwnProperty("private_constructor") ? function() { throw new Error("This class's constructor is private!"); } : function() { };

	/**
	 * Pole zaznamenávající které objekty třída implementuje
	 * @type {Array}
	 * @private
	 */
	var implementations = [];

	/**
	 * Ověřuje, zda třída používá dané rozhraní
	 * @param o
	 * @return {boolean}
	 */
	c.prototype.implementationOf = function(o) {
		if (o !== defs) {
			if (implementations.indexOf(o) != -1) {
				return true;
			}
		}

		return false;
	};

	/**
	 * Slouží k podědění třídy
	 * @param parent {Function} Třída
	 * @return {constructor}
	 */
	c.Extends = function(parent) {
		if (typeof parent === "function") {
			var Tmp = function() { };
			Tmp.prototype = parent.prototype;
			c._super = Tmp.prototype;
			c.prototype = new Tmp();
			c.prototype._super = function() {
				parent.apply(this, arguments);
			};

			// Musíme znovu nabalit vlastní metody, protože podědění nám smaže existující prototype - bohužel je to nevýhoda pozdního volání Extends
			c.Implements(defs);

			// Aktualizujeme
			c.prototype.constructor = c;
		}

		return c;
	};

	/**
	 * Slouží k převzení prvků z objektu/interface. Jedná se o mixing což se dá nazvat jako implementace rozhraní.
	 * @param obj {Object} Objekt/interface
	 * @return {constructor}
	 */
	c.Implements = function(obj) {
		if (obj !== defs) {
			if (implementations.indexOf(obj) != -1) {
				console.log("Toto rozhranní je již implementováno");
				return c;
			}

			implementations.push(obj);
		}

		// Přidáme vlastnosti na náš prototype
		var objs = Object.getOwnPropertyNames(obj);
		var count = objs.length;
		for (var p = 0; p < count; p++) {
			if (objs[p] != "constructor" && (typeof c.prototype[objs[p]] == "undefined" || obj === defs)) {
				c.prototype[objs[p]] = obj[objs[p]];
			}
		}

		return c;
	};

	// Nahodíme statické parametry na konstruktor
	if (typeof defs.static === "object") {
		for (var s in defs.static) {
			if (defs.static.hasOwnProperty(s)) {
				c[s] = defs.static[s];
			}
		}
	}

	// Nabalíme požadované metody na konstruktor, využijeme naší metody Implements
	c.Implements(defs);

	// Nastavíme constructor na konstruktor
	c.prototype.constructor = c;

	// Privátní konstruktor
	if (defs.hasOwnProperty("private_constructor")) {
		var Trida = defs.private_constructor;
		Trida.prototype = c.prototype;
		Trida.prototype.constructor = c;
		c.PrivateConstructor = Trida;
	}

	return c;
};
