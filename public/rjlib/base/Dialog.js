/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: Dialog.js                       ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Třída pro vytváření dialogových oken
 * @class
 */
jumbo.Dialog = _class({
	static: {
		upCount: 0
	},


	/**
	 * @param {Object} data Vstupní data
	 * @constructor
	 */
	constructor: function(data) {
		var self = this;

		/**
		 * Zaznamenává stav otevření okna
		 * @type {boolean}
		 */
		this.isOpen = false;


		/**
		 * Z-index okna
		 * @type {number}
		 */
		this.zIndex = 90000;


		/**
		 * Overlay okna s fixni pozicí - rodič this.el
		 */
		this.bs;


		/**
		 * Node okna
		 * @type {Node}
		 */
		this.el;


		/**
		 * Efekt použitý pro zobrazení okna
		 * @type {Function}
		 */
		this.openEffect = function(el) {
			jumbo.fadeIn(el, 300);
		};


		/**
		 * Efekt použitý pro skrytí okna
		 * @type {Function}
		 */
		this.closeEffect = function(el) {
			jumbo.fadeOut(el, 300, self.afterClose);
		};


		/**
		 * Akce provedená při zavření okna
		 * @type {undefined|function}
		 */
		this.onclose = data.onclose;


		/**
		 * Akce provedená po zavření okna
		 * @type {undefined|function}
		 */
		this.afterClose = data.afterClose;




		this.createWindow(data || {});
	},


	/**
	 * Zkonstruuje okno
	 * @param {{}} d
	 * @private
	 */
	createWindow: function(d) {
		var self = this;

		this.bs = /*jumbo.addEvent(*/jumbo.create("div", {
			style: "position:fixed;top:0;bottom:0;left:0;right:0;display: none;"
		})/*, "keypress", function(e) {
			e = e || window.event;
			var key = e.keyCode || e.which;

			if (key == 27) {
				if (self.zIndex == 90000 + jumbo.Dialog.upCount) {
					alert("zavřít");
				} else {
					alert(self.zIndex);
				}
			}
		})*/;//jumbo.getBlackScreen();

		if (!!d.up) {
			jumbo.Dialog.upCount++;
			this.zIndex = 90000 + jumbo.Dialog.upCount;
			this.bs.style.zIndex = this.zIndex;
		}

		this.el = jumbo.create("div", {
			class: "jumbo-dialog",
			content: [
				jumbo.create("div", { // TOP
					class: "top",
					content: [
						jumbo.create("span", {
							class: "title"
						}),
						jumbo.create("span", {
							class: "close"
						})
					]
				}),
				jumbo.create("div", { // MID
					class: "mid",
					content: [
						jumbo.create("div", {
							class: "tabs"
						}),
						jumbo.create("div", {
							class: "text"
						})
					]
				}),
				jumbo.create("div", {
					class: "bot"
				})
			]
		});

		// Nastavíme oknu požadovanou velikost
		if (!!d.width) {
			if (typeof d.width == "string") {
				this.el.style.width = d.width;
			} else {
				this.el.style.width = d.width + "px";
			}
		}

		if (!!d.height) {
			if (typeof d.height == "string") {
				this.el.style.height = d.height;
			} else {
				this.el.style.height = d.height + "px";
			}
		}

		this.bs.appendChild(this.el);

		// Přidáme element okna na začátek body
		jumbo.onReady(function() {
			document.body.insertBefore(self.bs, document.body.firstChild);

			// Okno dáme přibližně do středu stránky
			self.el.style.left = Math.round( ( document.body.scrollWidth) / 2 - d.width / 2) + "px";
		});

		// Při kliknutí na blackScreen nebo křížek se okno zavře
		if (!d.noclose) {
			// BlackScreen
			jumbo.addEvent(this.bs, "click", function(e) {
				e = e || window.event;
				var t = jumbo.eventTarget(e);

				// Pokud bylo kliknuto opravdu na blackScreen
				if (t == this) {
					self.close();
				}
			});

			// Tlačítko na zavření X
			this.el.children[0].children[1].onclick = function() {
				self.close();
			}
		} else {
			jumbo.hide(this.el.children[0].children[1]);
		}

		// Nastavíme oknu uživatelův class
		if (typeof d.class == "string") {
			jumbo.addClass(this.el, d.class);
		}

		// Do okna vložíme titulek
		if (typeof d.title == "string") {
			this.el.children[0].children[0].innerHTML = d.title;
		}

		// Pokud je požadováno, vůbec hlavičku nevykreslíme
		if (typeof d.notitle != "undefined") {
			jumbo.hide(this.el.children[0]);
		}

		if (typeof d.content != "undefined") {
			this.setContent(d.content);
		}

		// Nastavíme dragging
//		if (typeof d.draggable != "undefined" || d.draggable == true) {
//			jumbo.setDragging(this.el, this.el.children[0].children[0]);
//		}
	},


	/**
	 * Přenese okno do popředí
	 */
	focus: function() {
		this.bs.style.zIndex = 99999;
	},


	/**
	 * Přenese okno do pozadí
	 */
	blur: function() {
		this.bs.style.zIndex = this.zIndex;
	},


	/**
	 * Odebere okno z dokumentu
	 */
	destruct: function() {
		document.body.removeChild(this.bs);
	},


	/**
	 * Otevře okno
	 */
	open: function() {
		var self = this;
		if (!this.isOpen) {
			this.isOpen = true;

//			jumbo.fadeIn(this.bs, 300);
			this.openEffect(this.bs);
		}
	},


	/**
	 * Zavře okno
	 */
	close: function() {
		if (this.isOpen) {
			this.isOpen = false;

			if (typeof this.onclose == "function") {
				this.onclose();
			}

			this.closeEffect(this.bs);
//			jumbo.fadeOut(this.bs, 300);
		}
	},


	/**
	 * Změna titulku okna
	 * @param {string} title
	 */
	setTitle: function(title) {
		this.el.children[0].children[0].innerHTML = title;
	},


	/**
	 * Nastaví obsah do okna bez tabů
	 * @param {object} content
	 */
	setContent: function(content) {
		this.el.children[1].children[1].innerHTML = "";
		jumbo.appendCreate(this.el.children[1].children[1], content);
	},


	/**
	 * Přidáme obsah do okna bez tabů
	 * @param {object} content
	 */
	addContent: function(content) {
		jumbo.appendCreate(this.el.children[1].children[1], content);
	},


	/**
	 * Vrátí tab podle jména
	 * @param {string} tab Název tabu
	 * @returns {Node | null}
	 */
	getTab: function(tab) {
		for (var i = 0; i < this.el.children[1].children[0].children.length; i++) {
			if (this.el.children[1].children[0].children[i].innerHTML == tab) {
				return this.el.children[1].children[0].children[i];
			}
		}

		return null;
	},


	/**
	 * Vrátí element s obsahem náležící danému tabu
	 * @param {string} tab Název tabu
	 * @returns {Node | null}
	 */
	getContentTab: function(tab) {
		for (var i = 0; i < this.el.children[1].children[0].children.length; i++) {
			if (this.el.children[1].children[0].children[i].innerHTML == tab) {
				return this.el.children[1].children[1].children[i];
			}
		}

		return null;
	},


	/**
	 * Vrátí index otevřeného tabu
	 * @private
	 * @returns {number | null}
	 */
	getOpenTabIndex: function() {
		for (var i = 0; i < this.el.children[1].children[0].children.length; i++) {
			if (this.el.children[1].children[0].children[i].className == "tab-open") {
				return i;
			}
		}

		return null;
	},


	/**
	 * Zavře otevřený tab
	 */
	closeOpenTab: function() {
		var i = this.getOpenTabIndex();

		if (i === null) {
			return;
		}

		jumbo.setClass(this.el.children[1].children[0].children[i], "");
		jumbo.hide(this.el.children[1].children[1].children[i]);
	},


	/**
	 * Otevře daný tab
	 * @param {string} tab Název tabu
	 */
	openTab: function(tab) {
		this.closeOpenTab();
		jumbo.setClass(this.getTab(tab), "tab-open");
		jumbo.show(this.getContentTab(tab));
	},


	/**
	 * Přidá nový tab
	 * @param {string} tabName Název tabu
	 * @param {object} content Obsah
	 */
	addTab: function(tab, content) {
		content = content || null;
		var self = this;

		if (this.el.children[1].children[0].children.length == 0) {
			jumbo.show(this.el.children[1].children[0]);
		} else {
			this.closeOpenTab();
		}

		this.el.children[1].children[0].appendChild(jumbo.addEvent(jumbo.create("div", { content: tab, class: "tab-open" }), "click", function() {
			self.openTab(this.innerHTML);
		}));

		if (typeof content == "string" || typeof content == "object") {
			this.el.children[1].children[1].appendChild(jumbo.create("div", { content: content }));
		} else {
			this.el.children[1].children[1].appendChild(jumbo.create("div"));
		}
	},


	/**
	 * Odebere tab
	 * @param {string} tab Název tabu
	 */
	removeTab: function(tab) {
		this.el.children[1].children[0].removeChild(this.getTab(tab));
		this.el.children[1].children[1].removeChild(this.getContentTab(tab));
	},


	/**
	 * Změní název tabu
	 * @param {string} tab Název tabu
	 * @param {string} name
	 */
	changeTabName: function(tab, name) {
		this.getTab(tab).innerHTML = name;
	},


	/**
	 * Změní obsah tabu
	 * @param {Object} content
	 * @param {string} tab Název tabu
	 */
	setContentToTab: function(content, tab) {
		var c = this.getContentTab(tab);
		c.innerHTML = "";

		jumbo.appendCreate(c, content);
	}
});
