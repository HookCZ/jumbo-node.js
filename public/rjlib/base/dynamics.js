/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: dinamics.js                     ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/


/**
 * Vrací absolutní pozici elementu, nehledě na pozicování
 * @param {Node} obj Element jehož absolutní pozici v dokumentu chceme
 * @param [foundScrollLeft] nepoužívat
 * @param [foundScrollTop] nepoužívat
 * @returns {{x: number, y: number, isAbsPosition: boolean}}
 */
jumbo.absPos = function(obj, foundScrollLeft, foundScrollTop) {
	if (!jumbo.isNode(obj)) {
		if (jumbo.consoleLogging === true) console.log("Dosazený object není Node");
		return {x: null, y: null, isAbsPosition: false};
	}

	if (obj.getBoundingClientRect) {
		var rect = obj.getBoundingClientRect();

		return {
			x: rect.left,
			y: rect.top,
			isAbsPosition: true
		};
	}

	/* Následující kód vzat z: http://stackoverflow.com/questions/3930391/getting-absolute-position-of-an-element-relative-to-browser a následně poupraveno*/
	var curleft = 0;
	var curtop = 0;

	if (obj.offsetLeft) {
		curleft += parseInt(obj.offsetLeft);
	}
	if (obj.offsetTop) {
		curtop += parseInt(obj.offsetTop);
	}
	if (obj.scrollTop && obj.scrollTop > 0) {
		curtop -= parseInt(obj.scrollTop);
		foundScrollTop = true;
	}
	if (obj.scrollLeft && obj.scrollLeft > 0) {
		curleft -= parseInt(obj.scrollLeft);
		foundScrollLeft = true;
	}
	if (obj.offsetParent) {
		var pos = jumbo.absPos(obj.offsetParent, foundScrollLeft, foundScrollTop);
		curleft += pos.x;
		curtop += pos.y;
	} else if (obj.ownerDocument) {
		var thewindow = obj.ownerDocument.defaultView;

		if (!thewindow && obj.ownerDocument.parentWindow)
			thewindow = obj.ownerDocument.parentWindow;
		if (thewindow) {
			if (!foundScrollTop && thewindow.scrollY && thewindow.scrollY > 0) {
				curtop -= parseInt(thewindow.scrollY);
			}
			if (!foundScrollLeft && thewindow.scrollX && thewindow.scrollX > 0) {
				curleft -= parseInt(thewindow.scrollX);
			}
			if (thewindow.frameElement) {
				var pos = jumbo.absPos(thewindow.frameElement);
				curleft += pos.x;
				curtop += pos.y;
			}
		}
	}

	return {
		x:				curleft,
		y:				curtop,
		isAbsPosition:	true
	};
};


/**
 * Nastaví elementu absolutní pozici - umístění zůstane stejné
 * @param {Node} el Element, jehož pozice bude převedena na absolutní
 * @param {boolean} [totalAbsolute] Nepovinný parametr, v případě že bude true, tak element bude přesunut hned na počátek body
 * @returns {{x: number, y: number, isAbsPosition: boolean} | boolean}
 */
jumbo.setAbsPos = function(el, totalAbsolute) {
	if (!jumbo.isNode(el)) {
		if (jumbo.consoleLogging === true) console.log("Dosazený object není Node");
		return {x: null, y: null, isAbsPosition: false};
	}

	if (totalAbsolute === undefined || totalAbsolute !== true) {
		totalAbsolute = false;
	}

	var pos = jumbo.absPos(el);

	el.style.position = "absolute";
	el.style.margin = "0";
	el.style.right = "0";
	el.style.bottom = "0";

	// Fix CSSkového paradoxu, kdy absolute element není absolutní k dokumentu pokud se nachází v absolutně nebo relativně pozicovaném elementu
	if (totalAbsolute) {
		el.style.top = pos.y + "px";
		el.style.left = pos.x + "px";

		el.parentNode.removeChild(el);
		document.body.insertBefore(el, document.body.firstChild);
	} else {
		/* TODO: Správné vyřešení CSS paradoxu
		 ----------------------------------------------------------
		 Projít všechny předky elementu až k body a zjistit,
		 jestli mají nastavené position. Vezmeme nejbližší prvek,
		 zjistíme jeho absolutní pozici a tu odečteme od absPos() */

		var prnt = el.parentNode;
		while (prnt != document.body && window.getComputedStyle(prnt).position == "static") {
			prnt = prnt.parentNode;
		}

		if (prnt != document.body) {
			var abs = jumbo.absPos(prnt);

//			console.log("Pozice elementu", pos);
//			console.log("Pozice rodiče", abs);
//			console.log(prnt, window.getComputedStyle(prnt).position);

			el.style.top = (pos.y - abs.y) + "px";
			el.style.left = (pos.x - abs.x) + "px";
		} else {
			el.style.top = pos.y + "px";
			el.style.left = pos.x + "px";
		}
	}

	return pos;
};


/**
 * Vrátí pozici kurzoru
 * @param {Event} e Event
 * @returns {{x: number, y: number, isCurPosition: boolean}}
 */
jumbo.curPos = function(e) {
	e = e || window.event;

	var cur = {
		isCurPosition:	true,
		x:				parseInt(e.clientX),
		y:				parseInt(e.clientY),
		relative:		{
			x:	parseInt(e.clientX) + parseInt(document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft),
			y:	parseInt(e.clientY) + parseInt(document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)
		}
	};

	return cur;
};


/**
 * Přepne prohlížeč do full-screen režimu
 */
jumbo.toggleFullScreen = function() {
	var el = document.body;
	if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
		if (el.requestFullScreen) {
			el.requestFullScreen();
		} else if (el.mozRequestFullScreen) {
			el.mozRequestFullScreen();
		} else if (el.webkitRequestFullScreen) {
			el.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		} else if (el.msRequestFullscreen) {
			el.msRequestFullscreen();
		}
	} else {
		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
	}
};


/**
 * Znemožní označení myší nad daným elementem
 * @param {Element} target
 */
jumbo.disableSelection = function(target) {
	if (!jumbo.isElement(target)) {
		if (jumbo.consoleLogging === true) console.log("Dosazený object není Element");
		return;
	}

	if (typeof(target.onselectstart) != "undefined") { // IE
		target.onselectstart = function() {
			return false;
		}
	} else if (typeof(target.style.MozUserSelect) != "undefined") { // FireFox
		target.style.MozUserSelect = "none";
	} else { // Ostatní
		target.onmousedown = function(e) {
			e = e || window.event;
			var t = e.target || e.srcElement;

			if (t.tagName != "INPUT" && t.tagName != "TEXTAREA") {
				return false;
			}
		}
	}

	target.style.cursor = "default";
};


/**
 * Opět umožní označení myší nad daným elementem
 * @param {Element} target
 */
jumbo.enableSelection = function(target) {
	if (!jumbo.isElement(target)) {
		if (jumbo.consoleLogging === true) console.log("Dosazený object není Element");
		return;
	}

	if (typeof(target.onselectstart) != "undefined") { // IE
		target.onselectstart = function() {
			return true;
		}
	} else if (typeof(target.style.MozUserSelect) != "undefined") { // FireFox
		target.style.MozUserSelect = "";
	} else { // Ostatní
		target.onmousedown = function() {
			return true;
		}
	}

	target.style.cursor = "default";
};