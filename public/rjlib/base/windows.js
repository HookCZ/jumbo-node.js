/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: windows.js                      ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/


/**
 * Vlastní alert, který však sám zmizí po 4s
 * @param {object} text Text který se má zobrazit, může se jednat o create data
 * @param {string} [title] Titulek okna
 */
jumbo.flashAlert = function(text, title) {
	var w = new jumbo.Dialog({
		afterClose: function() {
			w.destruct();
			w = null;
		},
		class: "jumbo-dialog-alert",
		title: title,
		up: true
	});

	w.setContent(text);
	w.open();

	setTimeout(function() {
		if (!!w) {
			w.close();
		}
	}, 4000);
};


/**
 * Vlastní alert
 * @param {object} text Text který se má zobrazit, může se jednat o create data
 * @param {string} [title] Titulek okna
 */
jumbo.alert = function(text, title) {
	var w = new jumbo.Dialog({
		afterClose:	function() {
			w.destruct();
			w = null;
		},
		class:	"jumbo-dialog-alert",
		title:	title,
		up:		true
	});
	w.setContent(text);
	w.open();
};


/**
 * Vlastní confirm, který provede akci podle stisknutého tlačítka
 * @param {String} text Text který se zobrazí v okně, může se jednat o create data
 * @param {Array} buttons Pole stringů, každý string tvoří jedno tlačítko s daným textem
 * @param {Array|function} functions Pole funkcí náležících k daným tlačítkům, v případě že místo pole bude dosazena funkce, funkce se provede při výběru každé možnosti a text vybraného tlačítka bude dosazen jako parametr
 */
jumbo.confirm = function(text, buttons, functions) {
	if (Object.prototype.toString.call(buttons) != "[object Array]") {
		if (jumbo.consoleLogging === true) console.log("Špatné vstupní parametry.");
		return;
	}

	var w = new jumbo.Dialog({
		afterClose: function() {
			w.destruct();
			w = null;
		},
		class: "jumbo-dialog-confirm",
		noclose: true,
		up: true
	});

	var cont = jumbo.create("div", {
		class:	"buttons"
	});

	var func;
	var bl = buttons.length;
	var returnEventTrue;

	for (var i = 0; i < bl; i++) {
		if (typeof functions[i] == "function") {
			returnEventTrue = true;
			func = functions[i];
		} else if (typeof functions == "function") {
			returnEventTrue = false;
			func = functions;
		} else if (typeof functions[i] == "undefined") {
			if (jumbo.consoleLogging === true) console.log("Žádná akce nebyla tlačítku '" + buttons[i] + "' přiřazena.");
			func = null;
		}

		(function(i, func) {
			cont.appendChild(jumbo.addEvent(jumbo.create("button", {
				content:	buttons[i]
			}), "click", function(e) {

				if (func !== null) {
					if (returnEventTrue) {
						e = e || window.event;
						func(e);
					} else {
						func(buttons[i]);
					}
				}

				w.close();
//				w.destruct();
//				w = null;
			}));
		})(i, func);
	}

	w.setContent(jumbo.create("div", {
		class:		"wrap",
		content:	[
			text,
			cont
		]
	}));

	w.open();
};


/**
 * Vlastní prompt, volá callback se zadanou hodnotou
 * @param {String} text Text který se zobrazí v okně, může se jednat o create data
 * @param {String} defaultValue Výchozí hodnota
 * @param {function} callback Funkce která bude zavolána po odeslání hodnoty. Funkce bude volána s parametrem, a to se zadanou hodnotou, případně null, pokud bude zadání hodnoty zrušeno.
 */
jumbo.prompt = function(text, defaultValue, callback) {
	if (typeof defaultValue != "string" || typeof callback != "function") {
		if (jumbo.consoleLogging === true) console.log("Špatné vstupní parametry.");
		return;
	}

	var w = new jumbo.Dialog({
		afterClose: function() {
			w.destruct();
			w = null;
		},
		class: "jumbo-dialog-prompt",
		noclose: true,
		up: true
	});


	var input = jumbo.addEvent(jumbo.create("input", {
		type: "text",
		value: defaultValue
	}), "keypress", function(e) {
		e = e || window.event;
		var key = e.keyCode || e.which;

		if (key == 13) {
			callback(input.value);
			w.close();
		}
	});


	var cont = jumbo.create("div", {
		class:	"buttons"
	});

	cont.appendChild(jumbo.addEvent(jumbo.create("button", {
		content: "Potvrdit"
	}), "click", function(e) {
		callback(input.value);
		w.close();
	}));

	cont.appendChild(jumbo.addEvent(jumbo.create("button", {
		content: "Zrušit"
	}), "click", function(e) {
		callback(null);
		w.close();
	}));


	w.setContent(jumbo.create("div", {
		class: "wrap",
		content: [
			text,
			input,
			cont
		]
	}));

	w.open();
};