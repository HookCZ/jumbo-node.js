/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: app.js                          ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

jumbo.createController = (function() {
	function getFn(fn, callback) {
		fn = fn.match(/^([a-zA-Z0-9]*)(\((.*)\))?$/);

		if (fn !== null) {
			var args = [];
			if (!!fn[3]) {
				args = eval("[" + fn[3] + "]");
			}

			callback(fn[1], args);
		}
	}

	return function(template, Controller) {
		jumbo.onReady(function() {
			template = jumbo.get(template);

			if (template instanceof NodeList && template.length == 1) {
				template = template[0];
			}

			if (!(template instanceof Node)) {
				console.log("Error in jumbo.createController(), bad app Node");
				return;
			}


			/** Projdeme template a najdeme si všechny prvky pro nás */
			var els = template.getElementsByTagName("*");
			var ell;
			var event;
			var jEls = {
				events: [],
				content: null,
				links: [],
				forms: []
			};

			/**
			 * Zpracování elementů dokumentu pro inicializaci data-j prvků
			 * @param els
			 */
			var proccess = function(els) {
				for (var el in els) {
					if (els.hasOwnProperty(el)) {
						ell = els[el];

						if (!!ell.dataset) {
							for (var ds in ell.dataset) {
								if (ell.dataset.hasOwnProperty(ds) && ds.substr(0, 3) == "jOn") {
									jEls.events.push({element: ell, event: ds.substr(3), method: ell.dataset[ds]});
								}
							}

							// ODKAZY
							if (ell.dataset.hasOwnProperty('jLink')) {
								if (!ell.href || ell.tagName.toLowerCase() != "a") continue;
								jEls.links.push(ell);
							}

							// CONTENT
							if (ell.dataset.hasOwnProperty('jContent')) {
								jEls.content = ell;
							}

							// FORM
							if (ell.dataset.hasOwnProperty('jForm')) {
								if (!ell.action || ell.tagName.toLowerCase() != "form") continue;
								jEls.forms.push(ell);
							}
						}
					}
				}
			};

			/**
			 * Zpracování HTML dat
			 * @param data
			 * @param [title] Nepovinné jen pokud nechceme přidávat záznam do historie
			 * @param [href] Nepovinné stejně jako title
			 */
			var proccessPage = function(data, title, href) {
				var scripts = "";

				data.replace(/<script>([\s\S]*?)<\/script>/i, function(_, match) {
					scripts += match;
				});

				var odata = data;
				data = jumbo.get("[data-j-content]", jumbo.create("div", {
					content: data
				}))[0];

				if (title) {
					history.pushState({jPage: true, content: (odata || "")}, title, href);
					odata = null;
					document.title = title[1];
				}

				if (data instanceof Node) {
					jEls.content.innerHTML = data.innerHTML;
					proccess(jEls.content.getElementsByTagName("*"));
					registerActions();
					eval(scripts);
				} else {
					console.log("Data are in bad format.", " Data:", data);
				}
			};

			/**
			 * Registrace událostí na nalezené prvky z proccess()
			 */
			var registerActions = function() {};


			proccess(els);


			// Zaregistrujeme window event - požadavky o back / forward
			jumbo.addEvent(window, "popstate", function() {
				if (history.state && history.state.jPage) {
					if (!(jEls.content instanceof Node)) {
						console.log("Error: data-j-content not found.");
						return;
					}

					proccessPage(history.state.content);
				} else {
					Controller.prototype.loadPage(location.href, false);
				}
			});


			/** Na kontrolér přidáme nějaké vlastní akce */

			/**
			 * Načtení stránky
			 * @param href
			 * @param pushToHistory
			 */
			Controller.prototype.loadPage = function(href, pushToHistory) {
				if (!(jEls.content instanceof Node)) {
					console.log("Error: data-j-content not found.");
					return;
				}

				if (pushToHistory !== false) {
					pushToHistory = true;
				}

				jEls.content.innerHTML = "<div class='jumbo-loading'><div></div></div>";

				jumbo.xhr.get(href, function(err, odata) {
					if (err != null) {
						console.log("Error:", err.message);
						jEls.content.innerHTML = "Error occurs";
						return;
					}

					var data = odata;
					var title = data.match(/<(?:(?:title)|(?:TITLE)).*?>([\s\S]*?)<\/(?:(?:title)|(?:TITLE))>/);
					data = data.match(/<(?:(?:body)|(?:BODY)).*?>([\s\S]*?)<\/(?:(?:body)|(?:BODY))>/);

					if (data !== null) {
						if (!pushToHistory) {
							title = null;
							href = null;
						}
						proccessPage(data[1], title, href);
					} else {
						if (controller['proccessLoadPage']) {
							controller['proccessLoadPage'](odata);
						}
					}
				});
			};

			/**
			 * Odeslání formuláře
			 * @param form
			 */
			Controller.prototype.sendForm = function(form) {
				if (form.tagName.toLowerCase() != "form" || !form.action) {
					console.log("Data-j-form attribut were set on bad Element or just action miss.");
					return;
				}

				var self = this;

				jumbo.xhr.post(form.action, new FormData(form), function(err, data) {
					if (err != null) {
						console.log("Error:", err.message);
						return;
					}

					if (data.formErrorMessages) {
						var erl = data.formErrorMessages.length;

						for (var e = 0; e < erl; e++) {
							!!jumbo.alert ? jumbo.alert(data.formErrorMessages[e]) : alert(data.formErrorMessages[e]);
						}

						return;
					}

					if (data.redirect) {
						self.loadPage(data.redirect);
						return;
					}

					if (controller['proccessForm']) {
						controller['proccessForm'](data);
					}
				}, "json");
			};


			// Vytvoříme instanci kontroléru
			var controller = new Controller(jumbo.xhr || {post: function() {}, get: function() {}, put: function() {}, delete: function() {}});

			registerActions = function() {
				// Projdeme akce
				var eln = jEls.events.length;
				for (var e = 0; e < eln; e++) {
					event = jEls.events[e];
					getFn(event.method, function (fnName, args) {
						var called = false;
						jumbo.addEvent(event.element, event.event, function (e) {
							if (!called) {
								args.push(this);
								args.push(e || window.event);
								called = true;
							} else {
								args[args.length - 2] = this;
								args[args.length - 1] = e || window.event;
							}
							return controller[fnName].apply(controller, args);
						});
					});
				}


				// Odkazy
				eln = jEls.links.length;
				for (e = 0; e < eln; e++) {
					event = jEls.links[e];
					event.setAttribute("onclick", "return false;");
					jumbo.addEvent(event, "click", function () {
						controller.loadPage(this);
					});
				}


				// Formuláře
				eln = jEls.forms.length;
				for (e = 0; e < eln; e++) {
					event = jEls.forms[e];
					event.setAttribute("onsubmit", "return false;");
					jumbo.addEvent(event, "submit", function () {
						controller.sendForm(this);
					});
				}


				jEls.forms = [];
				jEls.links = [];
				jEls.events = [];
			};

			registerActions();
		});
	};
})();