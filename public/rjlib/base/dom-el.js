/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: dom-el.js                       ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Namespace pro metody vytvářející html elementy
 * @namespace
 * @requires jumbo.dom
 */
jumbo.el = {};


/**
 * @alias
 * @type {}
 */
$el = jumbo.el;


/**
 * Vytvoření DIV elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.div = function(a, cont) {
	return jumbo.create("div", a, cont);
};

/**
 * Vytvoření SPAN elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.span = function(a, cont) {
	return jumbo.create("span", a, cont);
};

/**
 * Vytvoření A elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.a = function(a, cont) {
	return jumbo.create("a", a, cont);
};

/**
 * Vytvoření H1 elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.h1 = function(a, cont) {
	return jumbo.create("h1", a, cont);
};

/**
 * Vytvoření H2 elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.h2 = function(a, cont) {
	return jumbo.create("h2", a, cont);
};

/**
 * Vytvoření H3 elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.h3 = function(a, cont) {
	return jumbo.create("h3", a, cont);
};

/**
 * Vytvoření P elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.p = function(a, cont) {
	return jumbo.create("p", a, cont);
};

/**
 * Vytvoření STRONG elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.strong = function(a, cont) {
	return jumbo.create("strong", a, cont);
};

/**
 * Vytvoření EM elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.em = function(a, cont) {
	return jumbo.create("em", a, cont);
};

/**
 * Vytvoření HEADER elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.header = function(a, cont) {
	return jumbo.create("header", a, cont);
};

/**
 * Vytvoření HGROUP elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.hgroup = function(a, cont) {
	return jumbo.create("hgroup", a, cont);
};

/**
 * Vytvoření MAIN elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.main = function(a, cont) {
	return jumbo.create("main", a, cont);
};

/**
 * Vytvoření NAV elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.nav = function(a, cont) {
	return jumbo.create("nav", a, cont);
};

/**
 * Vytvoření FOOTER elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.footer = function(a, cont) {
	return jumbo.create("footer", a, cont);
};

/**
 * Vytvoření SECTION elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.section = function(a, cont) {
	return jumbo.create("section", a, cont);
};

/**
 * Vytvoření ARTICLE elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.article = function(a, cont) {
	return jumbo.create("article", a, cont);
};

/**
 * Vytvoření IMG elementu
 * @param {Object | String} a Atributy
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Node}
 */
jumbo.el.img = function(a, cont) {
	return jumbo.create("img", a, cont);
};