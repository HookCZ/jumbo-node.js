/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: dom.js                          ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Ověřuje zda daný objekt je html element typu Node
 * @param {Object} el Object k ověření
 * @returns {Boolean}
 */
jumbo.isNode = function(el) {
	if (typeof el != "object") {
		return false;
	}

	if (window.Node) {
		return el instanceof Node;
	} else {
		return typeof el.innerHTML === "string" && typeof el.setAttribute === "function";
	}
};


/**
 * Ověřuje zda daný objekt je html element ype Element
 * @param {Object} el Object k ověření
 * @returns {Boolean}
 */
jumbo.isElement = function(el) {
	if (typeof el != "object") {
		return false;
	}

	if (window.Element) {
		return el instanceof Element;
	} else {
		return typeof el.innerHTML === "string" && typeof el.setAttribute === "function";
	}
};


/**
 * Vrátí HTML elementu nebo konstrukce v poli
 * @param {Array|Element} el
 * @returns {string}
 */
jumbo.getHtmlOf = function(el) {
	var wrap = document.createElement("div");

	if (Object.prototype.toString.call(el) == "[object Array]") {
		alert("Tato možnost zatím není implementovaná.");
	} else {
		wrap.appendChild(el);
	}

	return wrap.innerHTML;
};


/**
 * Převede HTML na objekt(y)
 * @param {String} html
 * @returns {NodeList | Node | null}
 */
jumbo.constructHTML = function(html) {
	if (typeof html !== "string") {
		if (jumbo.consoleLogging) console.log("Dosazený parametr není string.");
		return null;
	}

	var el = document.createElement("div");
	el.innerHTML = html;

	var child = el.childNodes;
	var cl = child.length;

	if (cl > 1) {
		return child;
	} else if (cl == 1) {
		return child[0];
	} else {
		return null;
	}
};


/**
 * Přidává obsah do elementu
 * @param {Node} el Objekt do kterého přidáme obsah
 * @param {Object} p Objekt který bude přidán
 */
jumbo.appendCreate = function(el, p) {
	var tmp;
	var ch;
	var child;
	var childl;

	if (jumbo.isNode(p)) {
		el.appendChild(p);
	} else if (Object.prototype.toString.call(p) == "[object Array]") {
		var pl = p.length;

		for (var e = 0; e < pl; e++) {
			jumbo.appendCreate(el, p[e]);
		}
	} else {
		tmp = el.cloneNode(false);
		tmp.innerHTML = p;
		child = tmp.childNodes;
		childl = child.length;

		for (ch = 0; ch < childl; ch++) {
			el.appendChild(child[0]);
		}

		tmp = null;
	}

	return el;
};


/**
 * Vytvoření html elementu
 * @param {String} tag
 * @param {Object | String} [params]
 * @param {Object | String} [cont] Obsah elementu
 * @returns {Element|NodeList}
 */
jumbo.create = function(tag, params, cont) {
	var el = null;

	if (typeof tag === "string") {
		if (tag.substr(0, 1) != "<") {
			if (tag == "svg") {
				if (document.createElementNS) {
					el = document.createElementNS("http://www.w3.org/2000/svg", "svg");
				} else {
					el = document.createElement("svg");
				}
				el.setAttribute("version", "1.2");
			} else {
				try {
					el = document.createElement(tag);
				} catch (e) {
//					if (e instanceof InvalidCharacterError) {
						console.log("Neznámý tag. Takový element nelze vytvořit.");
//					}
				}
			}

			var tmp;

			if (typeof params !== "object" && !!params) {
				params = jumbo.getAttributesAsObject(params);
			} else if (params == null) {
				params = {};
			}

			if (typeof params === "object") {

				if (typeof params.content === "undefined") {
					params.content = cont || "";
				}

				for (var a in params) {
					if (params.hasOwnProperty(a)) {
						if (a == "content") {
							jumbo.appendCreate(el, params[a]);
//							if (jumbo.isNode(params[a])) {
//								el.appendChild(params[a]);
//							} else if (Object.prototype.toString.call(params[a]) == "[object Array]") {
//								for (var e = 0; e < params[a].length; e++) {
//									if (jumbo.isNode(params[a][e])) {
//										el.appendChild(params[a][e]);
//									} else {
//										tmp = document.createElement(tag);
//										tmp.innerHTML = params[a][e];
//
//										var l = tmp.childNodes.length;
//
//										for (var ch = 0; ch < l; ch++) {
//											el.appendChild(tmp.childNodes[0]);
//										}
//
//										tmp = null;
//									}
//								}
//							} else {
//								el.innerHTML = params[a];
//							}
						} else {
							el.setAttribute(a, params[a]);
						}
					}
				}
			}
		} else {
			el = jumbo.constructHTML(tag);
		}
	} else {
		if (jumbo.consoleLogging === true) console.log("Tag musí být string");
	}

	return el;
};


/**
 * Převod textové reprezentace atributů na objektovou
 * @param {String} attributes
 */
jumbo.getAttributesAsObject = function(attributes) {
	if (typeof attributes !== "string") {
		if (jumbo.consoleLogging === true) {
			console.log("Z takového vstupu není možné sestavit argumenty.", attributes, " Voláno z:", arguments.callee.caller, "=> voláno z:", arguments.callee.caller.caller);
		}

		return {};
	}

	var obj = {};
	var attrs, i;

	if (jumbo.attrsTextRepresentation == "CSV") {
		var key, value;
		attrs = attributes.split(",");

		for (i = 0; i < attrs.length; i++) {
			key = attrs[i].split("=", 1)[0];
			value = attrs[i].substr(key.length + 1);

			if (key.match(/^[a-zA-Z-]+$/)) {
				obj[key] = value;
			} else {
				if (jumbo.consoleLogging === true) {
					console.log("Název atributu \"" + key + "\" je chybný");
				}
			}
		}
	} else if (jumbo.attrsTextRepresentation == "HTML") {
		attributes.replace('"', "'");
		attrs = attributes.split("' ");

		// Z posledního atributu odstraníme apostrof na konci
		var lastIndex = attrs.length - 1;
		var last = attrs[lastIndex];
		attrs[lastIndex] = last.substr(0, last.length - 1);

		var keyValue;

		for (i = 0; i < attrs.length; i++) {
			keyValue = attrs[i].split("='");

			if (keyValue[0].match(/^[a-zA-Z-]+$/)) {
				obj[keyValue[0]] = keyValue[1];
			} else {
				if (jumbo.consoleLogging === true) {
					console.log("Název atributu \"" + keyValue[0] + "\" je chybný");
				}
			}
		}
	}

	return obj;
};


/**
 * Nastaví class, původní přepíše
 * @param {Element} el
 * @param {string} cls class
 * @returns {Element}
 */
jumbo.setClass = function(el, cls) {
	if (!!el.tagName) {
		el.setAttribute("class", cls);
	}

	return el;
};


/**
 * Přidává class k existujícím
 * @param {Element} el
 * @param {string} cls class
 * @returns {Element}
 */
jumbo.addClass = function(el, cls) {
	if (jumbo.isNode(el) && !(new RegExp(cls + "( |$)")).test(el.className)) {
		jumbo.setClass(el, (el.className != "") ? el.className + " " + cls : cls);
	}

	return el;
};


/**
 * Odebere z třídu
 * @param {Element} el
 * @param {string} cls
 * @returns {Element}
 */
jumbo.removeClass = function(el, cls) {
	if (el.className == "") {
		return el;
	}

	jumbo.setClass(el, el.className.replace(new RegExp("(^" + cls + "( |$))|( " + cls + "$)|(" + cls + " )"), function() {
		return "";
	}));

	return el;
};


/**
 * Přidá nebo odebere třídu z elementu podle toho jestli ji už obsahuje nebo ne
 * @param {Element} el
 * @param {String} cls
 * @returns {Element}
 */
jumbo.toggleClass = function(el, cls) {
	if (new RegExp("(^" + cls + "( |$))|( " + cls + "$)|(" + cls + " )").test(el.className)) {
		return jumbo.removeClass(el, cls);
	}

	return jumbo.addClass(el, cls);
};


/**
 * Nastavuje ID
 * @param {Element} el
 * @param {string} id
 * @returns {Element}
 */
jumbo.setID = function(el, id) {
	if (jumbo.isNode(el)) {
		el.setAttribute("id", id);
	}

	return el;
};


/**
 * Vytvoří umělý checkbox s callback fcí
 * @param {function} callback
 * @param {boolean} defaultState Defaultní stav
 * @returns {Element}
 */
jumbo.checkbox = function(callback, defaultState) {
	var stav = defaultState || false;
	var c = jumbo.addEvent(jumbo.create("div", {
		class: "jumbo-checkbox"
	}), "click", function(e) {
		if (this.className.indexOf("disabled") != -1) {
			return;
		}

		if (stav) {
			jumbo.removeClass(c, "yes");
			stav = false;
		} else {
			jumbo.addClass(c, "yes");
			stav = true;
		}

		callback(jumbo.checkboxControl(c));
	});

	if (stav == true) {
		jumbo.addClass(c, "yes");
	}

	return c;
};


/**
 * Vrátí bobject s akcemi pro daný daný checkbox
 * @param el
 * @returns {*}
 */
jumbo.checkboxControl = function(el) {
	if (el.tagName.toLowerCase() != "div" || el.className.indexOf("jumbo-checkbox") == -1) {
		return null;
	}

	return {
		element: el,

		disable: function() {
			jumbo.addClass(el, "disabled");
		},

		enable: function() {
			jumbo.removeClass(el, "disabled");
		},

		check: function() {
			jumbo.addClass(el, "yes");
		},

		uncheck: function() {
			jumbo.removeClass(el, "yes");
		},

		isChecked: function() {
			return el.className.indexOf("yes") == -1;
		}
	}
};


/**
 * Aktivuje požadovanou událost na elementu
 * @param el
 * @param event
 */
jumbo.triggerEvent = function(el, event) {
	var evt;
	if (!!CustomEvent && !!(evt = new CustomEvent(event, {bubbles: true, cancelable: true}))) {
		el.dispatchEvent(evt);
	} else if ("createEvent" in document) {
		evt = document.createEvent("HTMLEvents");
		evt.initEvent(event, true, true);
		el.dispatchEvent(evt);
	} else {
		el.fireEvent("on" + event);
	}
};


/**
 * Všechny checkboxy shodující s výběrem budou nahrazeny za custom checkboxy
 * @param query
 */
jumbo.replaceCheckboxes = function(query) {
	var checkboxes = jumbo.get(query);
	var chl = checkboxes.length;

	for (var ch = 0; ch < chl; ch++) {
		(function(checkbox) {
			var jCheckbox = jumbo.checkbox(function() {
				if (jCheckbox.className.indexOf("disabled") != -1) {
					return;
				}

				checkbox.checked = !checkbox.checked;// ? "" : "checked";
				jumbo.triggerEvent(checkbox, "change");
			}, checkbox.checked);

			if (checkbox.nextSibling === null) {
				checkbox.parentNode.appendChild(jCheckbox);
			} else {
				checkbox.parentNode.insertBefore(jCheckbox, checkbox.nextSibling);
			}

			checkbox.style.visibility = "hidden";
			checkbox.style.position = "absolute";
		})(checkboxes[ch]);
	}
};