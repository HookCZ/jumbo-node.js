/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.cz                                 *
 ***************************************************
 * FileName: prototype-edit.js               ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

if (jumbo.prototypeEdit) {
	/**
	 * Převede string na celé číslo
	 * @returns {Number}
	 */
	String.prototype.toInt = function() {
		return parseInt(this);
	};


	/**
	 * Převede string na desetinné číslo
	 * @returns {Number}
	 */
	String.prototype.toFloat = function() {
		return parseFloat(this);
	};


	/**
	 * Přídá event na element
	 * @param {String} type
	 * @param {Function} fn
	 * @returns {Node}
	 */
	Node.prototype.on = function(type, fn) {
		return jumbo.addEvent(this, type, fn);
	};
	Window.prototype.on = function(type, fn) {
		return jumbo.addEvent(this, type, fn);
	};


	/**
	 * Odebere event z elementu
	 * @param type
	 * @param fn
	 * @returns {Node}
	 */
	Node.prototype.removeEvent = function(type, fn) {
		return jumbo.removeEvent(this, type, fn);
	};
	Window.prototype.removeEvent = function(type, fn) {
		return jumbo.removeEvent(this, type, fn);
	};


	/**
	 * Zobrazí element
	 * @returns {Node}
	 */
	Node.prototype.show = function() {
		return jumbo.show(this);
	};


	/**
	 * Skryje element
	 * @returns {Node}
	 */
	Node.prototype.hide = function() {
		return jumbo.hide(this);
	};


	/**
	 * Zobrazí element prolnutím
	 * @param {number} [time] Čas v ms za který má být element plně viditelný
	 * @param {function} [func]
	 * @returns {Node}
	 */
	Node.prototype.fadeIn = function(time, func) {
		return jumbo.fadeIn(this, time, func);
	};


	/**
	 * Skryje element prolnutím
	 * @param {number} [time] Čas v ms za který má být element plně skrytý
	 * @param {function} [func]
	 * @returns {Node}
	 */
	Node.prototype.fadeOut = function(time, func) {
		return jumbo.fadeOut(this, time, func);
	};


	/**
	 * Nastaví class elementu, původní přepíše
	 * @param {String} cls
	 * @returns {Node}
	 */
	Element.prototype.setClass = function(cls) {
		return jumbo.setClass(this, cls);
	};


	/**
	 * Přidá class na element, původní zachová
	 * @param {String} cls
	 * @returns {Node}
	 */
	Element.prototype.addClass = function(cls) {
		return jumbo.addClass(this, cls);
	};


	/**
	 * Přidá nebo odebere třídu z elementu podle toho jestli ji už obsahuje nebo ne
	 * @param {String} cls
	 */
	Element.prototype.toggleClass = function(cls) {
		return jumbo.toggleClass(this, cls);
	};


	/**
	 * Odebere danou class z elementu
	 * @param {String} cls
	 * @returns {Node}
	 */
	Element.prototype.removeClass = function(cls) {
		return jumbo.removeClass(this, cls);
	};


	/**
	 * Přidává obsah do elementu
	 * @param {Object} p Objekt který bude přidán
	 * @returns {Node}
	 */
	Element.prototype.appendCreate = function(p) {
		return jumbo.appendCreate(this, p);
	};


	/**
	 * Sroluje element - postupně mění height
	 * @param {Number} time
	 * @returns {Node}
	 */
	Element.prototype.roll = function(time) {
		return jumbo.roll(this, time);
	};


	/**
	 * Odroluje element - postupně mění height
	 * @param {Number} time
	 * @returns {Node}
	 */
	Element.prototype.unroll = function(time) {
		return jumbo.unroll(this, time);
	};
}