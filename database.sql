/*
MySQL Data Transfer
Source Host: 127.0.0.1
Source Database: jumbo
Target Host: 127.0.0.1
Target Database: jumbo
Date: 28.4.2015 23:49:55
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for permits
-- ----------------------------
CREATE TABLE `permits` (
  `permits_id` int(10) unsigned NOT NULL auto_increment,
  `roles_id` smallint(5) unsigned NOT NULL,
  `presenter` varchar(100) NOT NULL,
  `action` varchar(100) NOT NULL,
  PRIMARY KEY  (`permits_id`),
  KEY `fk_permits_roles_id` (`roles_id`),
  CONSTRAINT `fk_permits_roles_id` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`roles_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
CREATE TABLE `roles` (
  `roles_id` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY  (`roles_id`),
  UNIQUE KEY `index_roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users
-- ----------------------------
CREATE TABLE `users` (
  `users_id` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(30) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(40) NOT NULL,
  `roles_id` smallint(5) unsigned default '0',
  PRIMARY KEY  (`users_id`),
  KEY `fk_users_roles_id` (`roles_id`),
  CONSTRAINT `fk_users_roles_id` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`roles_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `permits` VALUES ('1', '1', 'Sign', 'show');
INSERT INTO `permits` VALUES ('2', '1', 'Sign', 'register');
INSERT INTO `permits` VALUES ('3', '1', 'Main', 'show');
INSERT INTO `roles` VALUES ('3', 'admin');
INSERT INTO `roles` VALUES ('1', 'guest');
INSERT INTO `roles` VALUES ('2', 'user');
INSERT INTO `users` VALUES ('1', 'user', 'user@localhost', '178e8f793558788b4be9a032ca021543d39a69e6', '2');
