/**
 * Defaultní spouštěcí soubor aplikace
 *
 * This file is part of Jumbo framework for Node.js
 * Written by Roman Jámbor
 */

// Vložíme loader, který načte celou aplikaci
var loader = require("./core/loader");

// Nastavení Locatoru - URL
var locator = loader.application.getLocator();

locator.setHost("jumbo.js"); // Slouží pro vytváření odkazů a detekci sub-apps
locator.setMainSubdomain("www"); // default, bude platit jak www.stranka.xx tak stranka.xx
locator.addSubdomain("admin"); // Zpřístupní admin.stranka.xx a bude směřovat do /app/sub-apps/admin - zatím není dokončeno

//locator.addLocation("registrace.html", "/", false, "Sign", "register", "Www");
//locator.addLocation("rest[/$schars]", "/", false, "Rest", "bridge", "Www");
locator.addLocation("$presenter/$action[/$all[/$all[/$all]]]", "/"); //"$presenter/$action(?:/([0-9]+)(?:/([a-z]+))?)?"

// Až bude aplikace připravena, spustíme server na požadovaném portu
loader.application.runWhenReady(80, function() {
	console.timeEnd("Application load-time: ");
});
