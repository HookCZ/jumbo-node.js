/**
 * Script, který projde složky Core a App a všechny třídy (podle názvu souboru) vloží do objektu.
 * Script soubory neotvírá, pouze čte jejich název. Soubory se načtou až při jeho volání.
 * Samozřejmě jsou soubory cachované díky require, a tak se soubor nenačítá při každém voláni znova.
 *
 * This file is part of Jumbo framework for Node.js
 * Written by Roman Jámbor
 */

// TODO: Do budoucna upravit implementaci, aby brala v potaz @namespace z daných souborů

var
	fileSystem = require("fs"),
	path = require("path");

// Přidáme podporu pro soubory .class
require.extensions['.class'] = require.extensions['.js'];

// Vytvoříme si fci pro rekurzivní volání
function loadDir(dir) {
	var result = {};
	var list = fileSystem.readdirSync(dir);

	list.forEach(function(fileName) {
		var file = path.resolve(dir, fileName);
		var stat = fileSystem.lstatSync(file);
		var name;

		if (stat) {
			if (stat.isDirectory()) {
				name = (fileName.charAt(0).toUpperCase() + fileName.substr(1)).replace(/-(.)/g, function(_, upChar) {
					return upChar.toUpperCase();
				});
				result[name] = loadDir(file);
			} else if (stat.isFile()) {
				// Pokud jde o třídu -> první písmeno je velké
				if (/^[A-Z].*\.((js)|(class))/.test(fileName)) {
					var cls = fileName.replace(/\.((js)|(class))$/, "");

					Object.defineProperty(result, cls, {
						get: function() {
							return require(file);
						}
					});

					// Budeme sledovat soubory ze složky app, pokud dojde ke změně, načteme jej znova
					if (file.substr(0, Jumbo.APP_DIR.length) == Jumbo.APP_DIR) {
						fileSystem.watch(file, function (event) {
							if (event != "change") return;
							Jumbo.Log.Log.line("File " + file + " changed - reload", Jumbo.Log.Log.STD, Jumbo.Log.Log.TALKATIVE);
							delete require.cache[file];
						});
					}
				}
			}
		}
	});

	return result;
}

module.exports = {
	Core:	loadDir(Jumbo.CORE_DIR),
	App:	loadDir(Jumbo.APP_DIR)
};
