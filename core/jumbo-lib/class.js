/**
 * Funkce pro tvorbu tříd
 * @returns {{
		class: Function,
		privateAccessor: Function,
		extends: Function,
		implements: Function,
		private: {
			constructor: Function
		}
	}}
 */
var Class = (function() {
	var classIdCounter = 0;
	var classPrivateProperties = {};

	return function(defs) {
		var classId = classIdCounter++;
		classPrivateProperties[classId] = {};
		var privateProperties = classPrivateProperties[classId];

		var idCounter = 0;
		var constructor;
		var hasPrivateConstructor = false;
		var ctor;

		var p = function(self) {
			return classPrivateProperties[self.classprivateCallIdentifier][self.privateCallIdentifier];
		};

		if (defs.hasOwnProperty("constructor")) {
			ctor = defs.constructor;
			constructor = function() {
				if (this.constructor == ctor) { // V případě že dědíme, můžeme volat super konstruktor => Pokud bychom ho zavolali, došlo by k namixování private identifikátorů
					privateProperties[idCounter] = {};

					// Uložíme privátní vlastnosti
					if (typeof defs.private === "object") {
						for (s in defs.private) {
							if (defs.private.hasOwnProperty(s) && s != "constructor") {
								privateProperties[idCounter][s] = defs.private[s];
							}
						}
					}

					this.privateCallIdentifier = idCounter++;
					this.classprivateCallIdentifier = classId;
				}

				defs.constructor.apply(this, arguments);
			};
		} else if (defs.hasOwnProperty("private") && defs.private.hasOwnProperty("constructor")) {
			ctor = function() { throw new Error("This class's constructor is private!"); };
			constructor = function() {
				privateProperties[idCounter] = {};

				// Uložíme privátní vlastnosti
				if (typeof defs.private === "object") {
					for (s in defs.private) {
						if (defs.private.hasOwnProperty(s) && s != "constructor") {
							privateProperties[idCounter][s] = defs.private[s];
						}
					}
				}

				this.privateCallIdentifier = idCounter++;
				this.classprivateCallIdentifier = classId;

				defs.private.constructor.apply(this, arguments);
			};

			hasPrivateConstructor = true;
		} else {
			ctor = function() {};
			constructor = function() {
				if (this.constructor == ctor) {
					privateProperties[idCounter] = {};

					// Uložíme privátní vlastnosti
					if (typeof defs.private === "object") {
						for (s in defs.private) {
							if (defs.private.hasOwnProperty(s) && s != "constructor") {
								privateProperties[idCounter][s] = defs.private[s];
							}
						}
					}
				}
				this.privateCallIdentifier = idCounter++;
				this.classprivateCallIdentifier = classId;
			}
		}

		// Pokud definice obsahují konstruktor, vytvoříme jej, jinak použijeme prázdnou fci
		var c = hasPrivateConstructor ? ctor : constructor;

		/**
		 * Pole zaznamenávající které objekty třída implementuje
		 * @type {Array}
		 * @private
		 */
		var implementations = [];

		var implementsFunc;
		var extendsFunc;
		var privateCtor;

		/**
		 * Ověřuje, zda třída používá dané rozhraní
		 * @param {Object} o
		 * @return {Boolean}
		 */
		var implementationFunc = function(o) {
			if (o !== defs) {
				if (implementations.indexOf(o) != -1) {
					return true;
				}
			}

			return false;
		};

		/**
		 * Slouží k podědění třídy
		 * @param {Function} parent Třída k podědění
		 * @return {{class: Function, privateAccessor: Function}}
		 */
		extendsFunc = function(parent) {
			if (typeof parent === "function") {
				c.prototype = Object.create(parent.prototype);
				c.super = parent.prototype;
				c.prototype.super = function() {
					return parent.apply(this, arguments);

					//parent.apply(this, arguments);
				};

				// Zkopírujeme také statické vlastnosti z předka
				for (var prop in parent) {
					if (parent.hasOwnProperty(prop) && prop != "super" && prop != "constructor") {
						c[prop] = parent[prop];
					}
				}

				// Musíme znovu nabalit vlastní metody, protože podědění nám smaže existující prototype - bohužel je to nevýhoda pozdního volání Extends
				implementsFunc(defs);

				c.prototype.implementationOf = implementationFunc;

				// Aktualizujeme
				c.prototype.constructor = ctor;
			}

			return {
				/**
				 * @type {Function}
				 */
				class: c,
				privateAccessor: p,
				extends: extendsFunc,
				implements: implementsFunc,
				private: {
					/**
					 * @type {Function}
					 */
					constructor: privateCtor
				}
			};
		};

		/**
		 * Slouží k převzení prvků z objektu/interface. Jedná se o mixing což se dá nazvat jako implementace rozhraní.
		 * @param obj {Object} Objekt/interface
		 * @return {{class: Function, privateAccessor: Function}}
		 */
		implementsFunc = function(obj) {
			if (obj !== defs) {
				if (implementations.indexOf(obj) != -1) {
					console.log("Toto rozhranní je již implementováno");
					return c;
				}

				implementations.push(obj);
			}

			// Přidáme vlastnosti na náš prototype
			var objs = Object.getOwnPropertyNames(obj);
			var count = objs.length;
			var objsp;
			for (var p = 0; p < count; p++) {
				objsp = objs[p];
				if (objsp != "static" && objsp != "private" && objsp != "constructor" && objsp != "private_constructor" && (typeof c.prototype[objsp] == "undefined" || obj === defs)) {
					if (obj !== defs) {
						if (!c.prototype[objsp]) {
							console.log("This Class does NOT implement all features from its Interface.");
						}
					}
					c.prototype[objsp] = obj[objsp];
				}
			}

			if (obj === defs && !c.prototype.hasOwnProperty("destructor")) {
				c.prototype.destruct = function() {
					delete privateProperties[this.privateCallIdentifier];
				};
			}

			return {
				/**
				 * @type {Function}
				 */
				class: c,
				privateAccessor: p,
				extends: extendsFunc,
				implements: implementsFunc,
				private: {
					/**
					 * @type {Function}
					 */
					constructor: privateCtor
				}
			};
		};

		var s;

		// Uložíme statické vlastnosti
		if (typeof defs.static === "object") {
			for (s in defs.static) {
				if (defs.static.hasOwnProperty(s)) {
					c[s] = defs.static[s];
				}
			}
		}

		// Nabalíme požadované metody na konstruktor, využijeme naší metody Implements
		implementsFunc(defs);

		// Nastavíme constructor na konstruktor
		c.prototype.constructor = ctor;
		c.constructor = ctor;

		c.prototype.implementationOf = implementationFunc;

		// Privátní konstruktor
		if (hasPrivateConstructor) {
			var Trida = constructor;
			Trida.prototype = c.prototype;
			Trida.constructor = ctor;
			//Trida.prototype.constructor = c;
			privateCtor = Trida;
		}

		return {
			/**
			 * @type {Function}
			 */
			class: c,
			privateAccessor: p,
			extends: extendsFunc,
			implements: implementsFunc,
			private: {
				/**
				 * @type {Function}
				 */
				constructor: privateCtor
			}
		};
	};
})();


module.exports = Class;