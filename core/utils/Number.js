/**
 * This file is part of Jumbo framework for Node.js
 * Written by Roman Jámbor
 */

/**
 * @namespace Jumbo.Utils
 */

/**
 * @memberOf Jumbo.Utils
 * @namespace Number
 * @type {{}}
 */
var number = {};


/**
 * Vytvoří řetězec čísla s doplněním nul na začátek
 * @param num
 * @param length
 * @returns {*}
 */
number.zeroFill = function(num, length) {
	var snum = num.toString();
	var ln = snum.length;
	var a = length - ln;

	if (a <= 0) {
		return snum;
	}

	var zeros = "000000000000000000000000000000000000".substr(0, (length - ln));

	if (num >= 0) {
		return zeros + snum;
	}

	return "-" + zeros + snum.substr(1);
};

module.exports = number;
