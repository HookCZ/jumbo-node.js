/**
 * This file is part of Jumbo framework for Node.js
 * Written by Roman Jámbor
 */


/**
 * Use namespace
 * @type {{}}
 */
var number = Jumbo.Utils.Number;


/**
 * @namespace Jumbo.Utils
 */


/**
 * @memberOf Jumbo.Utils
 * @namespace Hash
 * @type {{}}
 */
var hash = {};


/**
 * Vytvoří jednoduchý dekódovatelný hash
 * @param val
 * @param {Number} [maxLength] Určí požadavek na délku výsledného hashe
 */
hash.easyHash = function(val, maxLength) {
	maxLength = maxLength || 40;

	if (maxLength > 99) {
		maxLength = 99;
	}

	var x = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	var xl = x.length;
	var l = val.length;
	var rl = maxLength - 2;

	if (rl < l * 2) {
		rl = 2 * l;
	}

	var s = number.zeroFill(l, 2);
	var space = Math.floor(rl / l);
	var output = "";

	var index = l - 1;
	for (var i = 0; i < rl; i++) {
		if (i % space == 0) {
			output += val.charAt(index);
			index--;
			continue;
		}

		output += x[Math.floor(Math.random() * xl)];
	}

	return s.charAt(0) + output + s.charAt(1);
};


/**
 * Dekóduje easy hash
 * @param hash
 */
hash.decodeEasyHash = function(hash) {
	try {
		var hl = hash.length;
		var l = parseInt(hash.charAt(0) + hash.charAt(hl - 1));
		var space = Math.floor((hl - 2) / l);
		var out = "";

		for (var i = l - 1; i >= 0; i--) {
			out += hash.charAt(1 + i * space);
		}

		return out;
	} catch (ex) {
		return "";
	}
};


/**
 * Vytvoří sha1 hash
 * @param v
 * @returns {*}
 */
hash.sha1 = function(v) {
	var crypto = require('crypto');
	var shasum = crypto.createHash('sha1');
	shasum.update(v);
	return shasum.digest('hex');
};

module.exports = hash;
