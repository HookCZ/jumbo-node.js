/**
 * This file is part of Jumbo framework for Node.js
 * Written by Roman Jámbor
 */

/**
 * @namespace Jumbo.Utils
 */


/**
 * @namespace String
 * @memberOf Jumbo.Utils
 */
var string = {};

/**
 * Asynchronní nahrazení
 * @memberOf Jumbo.Utils.String
 * @param {String} input
 * @param {RegExp} regex
 * @param {Function} replacer Funkce, která bude provádět nahrazení
 * @param {Function} callback Callback, který bude zavolán při finálním dokončení
 * @example
 	string.asyncReplace("asyncreplace test", /([aeiou])/g, function(match, e, done) {
		setTimeout(function() {
			done(e.toUpperCase());
		}, 50);
	}, function(output) {
		console.log(output);
	});
 */
string.asyncReplace = function(input, regex, replacer, callback) {
	callback = callback || function() {};
	replacer = replacer || function() {};

	var syncQueue = [];
	var matches = [];

	try {
		input.replace(regex, function() {
			(function(args) {
				syncQueue.push(function(done) {
					args.splice(args.length - 2 , 0, function(out) {
						matches.push(out);
						done();
					});

					replacer.apply(null, args);
				});
			})(Array.prototype.slice.call(arguments));
		});
	} catch (ex) {
		callback(input);
		return;
	}

	if (syncQueue.length == 0) {
		callback(input);
		return;
	}

	syncQueue.push(function() {
		var count = 0;
		var output = input.replace(regex, function() {
			return matches[count++];
		});

		callback(output);
	});

	new Jumbo.Sync.Sync(syncQueue);
};


/**
 * Nahradí v řetězci nebezpečné html entity
 * @param text
 * @memberOf Jumbo.Utils.String
 * @returns {string}
 */
string.htmlSpecialChars = function(text) {
	var map = {
		'<': "&lt;",
		'>': "&gt;",
		'&': "&amp;",
		'"': "&quot;",
		"'": "&#039;"
	};

	return text.replace(/[&<>"']/g, function(m) { return map[m]; });
};


/**
 * Vytvoří nový řetězec, ve kterém nahraí diakritiku a jiné znaky; Výsledek lze použít pro URL jako odkaz na stránku/článek; Nehlídá si unikátnost oproti dříve vygenerovaným řetězcům
 * @param {String} text
 * @memberOf Jumbo.Utils.String
 * @returns {string}
 */
string.seoUrl = function(text) {
	var map = {
		'&': "at",
		'À': "a",
		'Á': "a",
		'Â': "a",
		'Ã': "a",
		'Ä': "a",
		'Å': "a",
		'Æ': "ae",
		'Ç': "c",
		'È': "e",
		'É': "e",
		'Ê': "e",
		'Ë': "e",
		'Ì': "i",
		'Í': "i",
		'Î': "i",
		'Ï': "i",
		'Ð': "d",
		'Ñ': "n",
		'Ò': "o",
		'Ó': "o",
		'Ô': "o",
		'Õ': "o",
		'Ö': "o",
		'Ù': "u",
		'Ú': "u",
		'Û': "u",
		'Ü': "u",
		'Ý': "y",
		'Ÿ': "y",
		'Č': "c",
		'Ď': "d",
		'Ě': "e",
		'Ĺ': "l",
		'Ľ': "l",
		'Ň': "n",
		'Ő': "o",
		'Ŕ': "r",
		'Š': "s",
		'Ť': "t",
		'Ů': "u",
		'Ű': "u",
		'Ř': "r",
		'Ž': "z",
		'à': "a",
		'á': "a",
		'â': "a",
		'ã': "a",
		'ä': "a",
		'å': "a",
		'æ': "ae",
		'ç': "c",
		'è': "e",
		'é': "e",
		'ê': "e",
		'ë': "e",
		'ì': "i",
		'í': "i",
		'î': "i",
		'ï': "i",
		'ð': "o",
		'ñ': "n",
		'ò': "o",
		'ó': "o",
		'ô': "o",
		'õ': "o",
		'ö': "o",
		'ù': "u",
		'ú': "u",
		'û': "u",
		'ü': "u",
		'ý': "y",
		'ÿ': "y",
		'č': "c",
		'ď': "d",
		'ě': "e",
		'ĺ': "l",
		'ľ': "l",
		'ň': "n",
		'ő': "o",
		'ŕ': "r",
		'š': "s",
		'ť': "t",
		'ů': "u",
		'ű': "u",
		'ř': "r",
		'ž': "z",
		'.': "",
		' ': "-",
		'+': "-",
		'\'': "",
		'"': "",
		'=': "",
		'?': "",
		'!': ""
	};

	return text.replace(/[&ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖÙÚÛÜÝŸČĎĚĹĽŇŐŔŠŤŮŰŘŽàáâãäåæçèéêëìíîïðñòóôõöùúûüýÿčďěĺľňőŕšťůűřž. +'"=?!]/g, function(m) { return map[m]; }).replace(/-{2,}/, "-").trim().toLowerCase();
};


/**
 * Escapuje řetězec pro použití v template -> nahrazuje složené závorky
 * @param text
 */
string.templateEscape = function(text) {
	return text.replace(/\{|}/g, function(m) {
		if (m == "{") return "{l}";
		return "{r}";
	});
};

module.exports = string;
