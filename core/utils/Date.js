/**
 * This file is part of Jumbo framework for Node.js
 * Written by Roman Jámbor
 */


/**
 * @namespace Jumbo.Utils
 */


/**
 * @class
 * @name Jumbo.Utils.Date
 * @memberOf Jumbo.Utils
 */
var date = _class(
	/**
	 * @lends Jumbo.Utils.Date.prototype
	 */
	{
		/**
		 * @constructs
		 */
		constructor: function () {
			this._super.apply(null, Array.prototype.slice.call(arguments));
		},

		/**
		 * Vrátí datum v zadaném formátu - PHP-like
		 * @see http://php.net/manual/en/function.date.php
		 * @param {string} format
		 * @returns {string}
		 */
		getFormat: function (format) {
			var out = "";
			var pl = format.length;
			var tmp;

			for (var i = 0; i < pl; i++) {
				switch (format.charAt(i)) {
					case "H":
						tmp = this.getHours();
						if (tmp.length != 2) tmp = "0" + tmp;
						out += tmp;
						break;

					case "i":
						tmp = this.getMinutes();
						if (tmp.length != 2) tmp = "0" + tmp;
						out += tmp;
						break;

					case "s":
						tmp = this.getSeconds();
						if (tmp.length != 2) tmp = "0" + tmp;
						out += tmp;
						break;

					case "d":
						tmp = this.getDate();
						if (tmp.length != 2) tmp = "0" + tmp;
						out += tmp;
						break;

					case "m":
						tmp = this.getMonth();
						if (tmp.length != 2) tmp = "0" + tmp;
						out += tmp;
						break;

					case "N":
						out += this.getMonth();
						break;

					case "D":
						out += ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][this.getDay()];
						break;

					case "l":
						out += ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][this.getDay()];
						break;

					case "n":
						out += [7, 1, 2, 3, 4, 5, 6][this.getDay()];
						break;

					case "L":
						var year = this.getFullYear();
						if ((year & 3) != 0) {
							out += 0;
							continue;
						}
						out += ((year % 100) != 0 || (year % 400) == 0) ? 1 : 0;
						break;

					case "w":
						out += this.getDay();
						break;

					case "S":
						out += (["st", "nd", "rd"][out.charAt(out.length - 1) - 1] || "th");
						break;

					case "j":
						out += this.getDate();
						break;

					case "z":
						var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
						var m = this.getMonth();
						var dayOfYear = dayCount[m] + this.getDate();

						if (m > 1 && this.getFormat("L") == 1) dayOfYear++;

						out += dayOfYear;
						break;

					case "Y":
						out += this.getFullYear();
						break;

					case "y":
						out += this.getFullYear().substr(2, 2);
						break;

					default:
						out += format.charAt(i);
				}
			}

			return out;
		},
	}).Extends(Date);

module.exports = date;