/**
 * This file is part of Jumbo framework for Node.js
 * Written by Roman Jámbor
 */

/**
 * @namespace Jumbo.Utils
 */


/**
 * @memberOf Jumbo.Utils
 * @namespace Object
 * @type {{}}
 */
var object = {};

/**
 * A function to calculate the approximate memory usage of objects
 * @description Created by Stephen Morley - http://code.stephenmorley.org/ - and released under the terms of the CC0 1.0 Universal legal code:
 * @link http://creativecommons.org/publicdomain/zero/1.0/legalcode
 * @param object
 * @returns {number}
 */
object.sizeof = function(object) {
	// initialise the list of objects and size
	var objects = [object];
	var size    = 0;

	// loop over the objects
	for (var index = 0; index < objects.length; index ++){

		// determine the type of the object
		switch (typeof objects[index]){

			// the object is a boolean
			case 'boolean': size += 4; break;

			// the object is a number
			case 'number': size += 8; break;

			// the object is a string
			case 'string': size += 2 * objects[index].length; break;

			// the object is a generic object
			case 'object':

				// if the object is not an array, add the sizes of the keys
				if (Object.prototype.toString.call(objects[index]) != '[object Array]'){
					for (var key in objects[index]) size += 2 * key.length;
				}

				// loop over the keys
				for (var key in objects[index]){

					// determine whether the value has already been processed
					var processed = false;
					for (var search = 0; search < objects.length; search ++){
						if (objects[search] === objects[index][key]){
							processed = true;
							break;
						}
					}

					// queue the value to be processed if appropriate
					if (!processed) objects.push(objects[index][key]);

				}
		}
	}

	// return the calculated size
	return size;
};


/**
 * Naklonuje object - odstraní reference
 * @param {Object} obj
 * @returns {*}
 */
object.clone = function(obj) {
	if (obj === undefined || obj === null) {
		throw new TypeError('Cannot convert undefined or null to object');
	}

	if (obj instanceof Date) { // Jedná se o Date
		return (new Date).setTime(obj.getTime());
	} else if (Object.prototype.toString.call(obj) === "[object Array]") { // Jedná se o Array
		var al = obj.length;
		var newArray = [];

		for (var a = 0; a < al; a++) {
			newArray.push(obj[a]);
		}

		return newArray;
	}


	// Pokud existuje Object.assign, použijeme to
	if (Object.assign) {
		return Object.assign({}, obj);
	}


	// Jedná se o Object
	var newObject = {};
	var props = Object.getOwnPropertyNames(obj);
	var pl = props.length;
	var propName;
	var prop;

	for (var p = 0; p < pl; p++) {
		propName = props[p];
		prop = obj[propName];

		if (prop == obj) {
			newObject[propName] = {};
		} else if (prop instanceof Object) {
			newObject[propName] = object.clone(prop);
		} else {
			newObject[propName] = prop;
		}
	}

	return newObject;
};

module.exports = object;