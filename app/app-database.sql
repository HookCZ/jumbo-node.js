SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for messages
-- ----------------------------
CREATE TABLE `messages` (
  `message_id` int(10) unsigned NOT NULL auto_increment,
  `text` varchar(255) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `pm` int(10) unsigned NOT NULL default '0',
  `date` varchar(14) NOT NULL,
  PRIMARY KEY  (`message_id`),
  KEY `user_id` (`user_id`),
  KEY `fk_messages_room_id1` (`room_id`),
  CONSTRAINT `fk_messages_room_id1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
CREATE TABLE `rooms` (
  `room_id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(40) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `date` varchar(14) NOT NULL,
  `locked` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users
-- ----------------------------
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `role` tinyint(3) unsigned NOT NULL default '0',
  `online` tinyint(3) unsigned NOT NULL default '0',
  `last_seen` varchar(14) default NULL,
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `users_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users_in_rooms
-- ----------------------------
CREATE TABLE `users_in_rooms` (
  `room_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`room_id`,`user_id`),
  KEY `fk_users_in_rooms_user_id` (`user_id`),
  CONSTRAINT `fk_users_in_rooms_room_id` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users_in_rooms_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
