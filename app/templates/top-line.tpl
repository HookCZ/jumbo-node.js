<div class="top-line">
	<header>
		<hgroup>
			<h1>Jumbo</h1>
			<h2>Node.js MVP framework for responzive RIA</h2>
		</hgroup>
		<nav>
			<ul>
				<li><a href="{link Main::show}">Domů</a></li>
				<li><a href="{link Main::show::me::product::20 'nazev=' + Math.round(10.454), 10, Math.round(20.5), 'x=and sure some string quoted param ;)', "test"}">Test link</a></li>
				{if $user.logged}
					<li><a href="{link Chat::roomsList}">Místnosti</a></li>
					<li><a href="{link Sign::logout}">Odhlásit se</a></li>
				{else}
					<li><a href="{link Sign::register}">Registrace</a></li>
					<li><a href="{link Sign::show}">Přihlášení</a></li>
				{/if}
			</ul>
		</nav>
	</header>
</div>
