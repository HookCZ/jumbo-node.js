{block content}
	<h2>{define title}Chat{/define}</h2>

	<div id="chat-messages-area">
		Načítání...
		{*<p>*}
			{*<span>Hawk: </span>Here's my test message*}
		{*</p>*}
		{*<p>*}
			{*<span>Hawk: </span>Tady je moje testovací zpráva*}
		{*</p>*}
		{*<p>*}
			{*<span>Hawk: </span>Tady je moje testovací zpráva*}
		{*</p>*}
	</div>
	<div id="chat-send-message-area">
		<input type="text">
	</div>
{/block}

{block head}
	<script>
		(function() {
			var roomID = "{$roomID}";
			var getMessagesLink = "{link Chat::getMessages::$roomID}";
			var sendMessageLink = "{link Chat::insertMessage}";
			var input = null;
			var msgBox = null;

			// Odeslání zprávy
			function sendMessage(text) {
				if (input === null || input.value == "" || /^\\w ([^ ]*) $/.test(input.value)) return;

				$.post(sendMessageLink, {mistnost: roomID, user: userID, text: text}).done(function(data) {
//					console.log("Příchozí data po vložení: ", data);
					if (data.error) {
						alert(data.error);
					}
				});

				input.value = "";

				// Pokud se jedná o PM tak upravíme text
				var matchPM = text.match(/^\\w (.*?) (.*)$/);
				if (!!matchPM) {
					var pro = matchPM[1];
					text = matchPM[2];
				}

				var date = new Date();

				// Přidáme zprávu do výpisu
				msgBox.appendChild($j.create("p", {
					content: [
						"[" + (date.getHours().toString().length == 1 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes().toString().length == 1 ? "0" + date.getMinutes() : date.getMinutes()) + ":" + (date.getSeconds().toString().length == 1 ? "0" + date.getSeconds() : date.getSeconds()) + "] ",
						$j.create("span", {
							content: userName + (!!pro ? " pro " + pro : "") + ": "
						}).addEvent("click", function() {
							sendMessageTo(userName);
						}),
						text
					]
				}));
			}

			// Vloží do inputu příznak soukromé zprávy
			function sendMessageTo(to) {
				if (input === null) return;

				var match = input.value.match(/^\\w (.*?) (.*)$/);
				if (!!match) {
					input.value = "\\w " + to + " " + match[2];
					return;
				}

				input.value = "\\w " + to + " " + input.value;
			}

			$j.onReadyCall(function() {

				input = $j.get("#chat-send-message-area").children[0];
				msgBox = $j.get("#chat-messages-area");

				// Načtení zpráv
				function updateMessages() {
					$.getJSON(getMessagesLink, function(data) {
						msgBox.innerHTML = "";

						var il = data.length;

						for (var i = 0; i < il; i++) {
							(function(data, i) {
								var date = new Date();
								date.setTime(data[i]['date']);

								msgBox.appendChild($j.create("p", {
									content: [
										"[" + (date.getHours().toString().length == 1 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes().toString().length == 1 ? "0" + date.getMinutes() : date.getMinutes()) + ":" + (date.getSeconds().toString().length == 1 ? "0" + date.getSeconds() : date.getSeconds()) + "] ",
										$j.create("span", {
											content: data[i]['name'] + ((!!data[i]['pro_name'] && data[i]['pro_name'] != "" && data[i]['pro_name'] != null) ? " pro " + data[i]['pro_name'] : "") + ": "
										}).addEvent("click", function() {
											sendMessageTo(data[i]['name']);
										}),
										data[i]['text']
									]
								}));
							})(data, i);
						}
					});
				}

				// Načteme zprávy a nastavíme interval pro obnovu
				updateMessages();
				setInterval(updateMessages, 3000);


				// Navážeme event na input
				/*$j.addEvent(input, "keyDown"*/$(input).on("keydown", function(e) {

					e = e || window.event;
					var key = e.which || e.keyCode;

					if (key == 13) {
						sendMessage(this.value);
					}
				});
			});
		})();
	</script>
{/block}