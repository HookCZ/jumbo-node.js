{block content}
	<h2>{define title}Seznam místností{/define}</h2>

	<div>
		<a href="{link Chat::createRoom}">Vytvořit novou místnost</a>
	</div>

	<ul class="chat-rooms-list">

		{debug $chatRooms}

		{foreach $chatRooms as $chatRoom}
			<li>
				{if $chatRoom['locked'] == 1}
					[Zamčeno]
				{else}
					<a href="{link Chat::show::$chatRoom['room_id']}">
				{/if}

				{$chatRoom['name']}

				{if $chatRoom['locked'] == 0}
					</a>
				{/if}

				{if $chatRoom['jsemClen']}
					| <a href="{link Chat::leaveRoom::$chatRoom['room_id']}">Opustit</a>
				{/if}

				{if $chatRoom['user_id'] == $user.id && $chatRoom['locked'] == 1}
					| <a href="{link Chat::odemknout::$chatRoom['room_id']}">Odemknout</a>
				{/if}

				{if $chatRoom['user_id'] == $user.id && $chatRoom['locked'] == 0}
					| <a href="{link Chat::zamknout::$chatRoom['room_id']}">Zamknout</a>
				{/if}
			</li>
		{/foreach}
	</ul>
{/block}