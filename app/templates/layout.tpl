<!DOCTYPE html>
<html {if $lang}lang="{$lang}"{/if}>
	<head>
		<meta charset="utf-8">
		<title>{defined title}{use title} | {/defined}Jumbo framework</title>
		<meta name='viewport' content='width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;'>
		<link rel="stylesheet" href="/jumbo-lib/jumbo.css">
		<link rel="stylesheet" href="/css/style.css">
		<link rel='stylesheet' media='(max-width: 8in)' href="/css/mobile.css">
		<script src="/js/jquery.js"></script>
		<script src="/js/rjlib.js"></script>
		<script>
			var $j = RJLib;
			{if $user.logged}
				var userID = {$user.id};
				var userName = "{$user.name}";
			{/if}
		</script>
		<script src="/js/script.js"></script>
		{include head}
		<script>
			{if $errorMessages}
				{foreach $errorMessages as $message}
					alert("{$message}");
				{/foreach}
			{/if}
		</script>
	</head>
	<body>
		{include "top-line.tpl"}

		<div class="container">
			<main>
				{include content}
			</main>
		</div>
	</body>
</html>
