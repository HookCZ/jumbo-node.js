<!DOCTYPE html>
<html {if $lang}lang="{$lang}"{/if}>
	<head>
		<meta charset="utf-8">
		<meta name='viewport' content='width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;'>


		<title>{defined title}{use title} | {/defined}Jumbo framework</title>


		{*<link rel="stylesheet" href="/jumbo-lib/jumbo.css">*}
		<link rel="stylesheet" href="/css/style.css">
		<link rel='stylesheet' media='(max-width: 8in)' href="/css/mobile.css">


		<script src="/js/jquery.js"></script>
		<script src="/js/rjlib.js"></script>

		<script src="rjlib/base/core.js"></script>
		<script src="rjlib/base/xhr.js"></script>
		<script src="rjlib/base/app.js"></script>
		<script src="rjlib/base/dom.js"></script>
		<script src="rjlib/base/effects.js"></script>
		<script src="rjlib/base/prototype-edit.js"></script>

		<script>
			var $j = RJLib;
			{if $user.logged}
				var userID = {$user.id};
				var userName = "{$user.name}";
			{/if}
		</script>
		<script src="/js/script.js"></script>

		{include head}

		<script>
			{if $errorMessages}
				{foreach $errorMessages as $message}
					alert("{$message}");
				{/foreach}
			{/if}
		</script>
	</head>
	<body>
		<div class="container">
			<main>
				{include content}
			</main>
		</div>
	</body>
</html>
