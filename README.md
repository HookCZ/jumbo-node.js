# Jumbo #

Jumbo is Node.js MVP framework for rich internet application (RIA).

Core of this framework is in this repo https://bitbucket.org/HookCZ/jumbo-core

This README will help You to start with this framework and it will tell you, partly, how to use that.

### UNDER DEVELOPMENT! ###

### What is Node.js and how to use that? ###
Everything is at Node.js's website, see http://nodejs.org.


### How to set up framework? ###
* Download/clone this repo
* Now You must add less, node-static, formidable and mysql2 modules -> to console in project dir: `npm install`
* Now You can start app via app.js -> in console: `node app.js`, there will be some errors cuz some folders are missing. You must create them on Your own cuz bitbucket don't show empty folders.
* Now, if You created all missing folders, You can start app.js. Your application will listen at port 80 (You can set another port in app.js) so go to http://127.0.0.1 and your application will be there.


### Description ###
Your folders are app and public.
In the public there are static files like css, images, client JavaScript etc.
In the app is Your application. There are presenters, templates and models folders and config.json file.


### TODO list ###
* HTTPS support
* Localizations - multi-language websites
* Multi-core support (Cluster)